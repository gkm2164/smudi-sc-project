import Dependencies._

name := "config-generator"

libraryDependencies ++= Seq(
  `typesafe-config`
)