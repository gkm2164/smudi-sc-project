package com.satreci.smudi.config

import java.io.{File, PrintWriter}

import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}

import scala.io.Source
import scala.util.matching.Regex

case class SMUDIConfigOptions(httpHost: String = "0.0.0.0", httpPort: Int,
                              hadoopHost: String, hadoopPort: Int,
                              sparkHost: String, sparkPort: Int,
                              clusterHost: String = "0.0.0.0", clusterPort: Int,
                              tileHost: String, tilePort: Int,
                              imdbHost: String, imdbPort: Int)

object ConfigGenerator {

  implicit val optionGenerator: Config => SMUDIConfigOptions =
    (config: Config) => {
      val HOST: String => String = (systemType) => s"smudi.$systemType.hostname"
      val PORT: String => String = (systemType) => s"smudi.$systemType.port"

      val httpHost = config.getString(HOST("http"))
      val httpPort = config.getInt(PORT("http"))
      val hadoopHost = config.getString(HOST("hadoop"))
      val hadoopPort = config.getInt(PORT("hadoop"))
      val sparkHost = config.getString(HOST("spark.backend"))
      val sparkPort = config.getInt(PORT("spark.backend"))
      val clusterHost = config.getString(HOST("cluster.remote"))
      val clusterPort = config.getInt(PORT("cluster.remote"))
      val tileHost = config.getString(HOST("spark.remote"))
      val tilePort = config.getInt(PORT("spark.remote"))
      val imdbHost = config.getString(HOST("persistence.remote"))
      val imdbPort = config.getInt(PORT("persistence.remote"))

      println(config.getStringList("smudi.cluster.seed-nodes"))

      SMUDIConfigOptions(httpHost, httpPort, hadoopHost, hadoopPort, sparkHost, sparkPort,
        clusterHost, clusterPort, tileHost, tilePort, imdbHost, imdbPort)
  }

  implicit val configGenerator: SMUDIConfigOptions => Config =
    (options: SMUDIConfigOptions) => {
      import options._
      val HOST: String => String = (systemType) => s"smudi.$systemType.hostname"
      val PORT: String => String = (systemType) => s"smudi.$systemType.port"

      val defaultConfig = ConfigFactory.load()

      import collection.JavaConverters._

      val list = Seq(s"akka.tcp://smudi-clusters@$clusterHost:$clusterPort")

      val map: Map[String, _] =
        Map(
          HOST("http") -> httpHost,
          PORT("http") -> httpPort,
          HOST("hadoop") -> hadoopHost,
          PORT("hadoop") -> hadoopPort,
          HOST("spark.backend") -> sparkHost,
          PORT("spark.backend") -> sparkPort,
          HOST("cluster.remote") -> clusterHost,
          PORT("cluster.remote") -> clusterPort,
          HOST("spark.remote") -> tileHost,
          PORT("spark.remote") -> tilePort,
          HOST("persistence.remote") -> imdbHost,
          PORT("persistence.remote") -> imdbPort,
          "smudi.hadoop.address" -> s"hdfs://$hadoopHost:$hadoopPort",
          "smudi.spark.address" -> s"spark://$sparkHost:$sparkPort",
          "smudi.cluster.seed-nodes" -> list,
          "smudi.cluster-seed.host" -> clusterHost,
          "smudi.cluster-seed.port" -> clusterPort
        )

      def seqToJavaList[T]: Seq[T] => java.util.List[T] = (input) => seqAsJavaListConverter(input).asJava

      map.foldLeft(defaultConfig)((config, tuple) => {
        tuple match {
          case (key, value: String) => config.withValue(key, ConfigValueFactory.fromAnyRef(value))
          case (key, value: Int) => config.withValue(key, ConfigValueFactory.fromAnyRef(value))
          case (key, value: Seq[_]) => config.withValue(key, ConfigValueFactory.fromAnyRef(seqToJavaList(value)))
        }
      })
  }

  val addrMatch: Regex = "(.*):([0-9]+)".r

  def parseOptions(args: List[String])(implicit basicOption: SMUDIConfigOptions): Config = {
    def parseOptions_(args: List[String], currentOption: SMUDIConfigOptions): SMUDIConfigOptions = {
      args match {
        case "--http" :: addrMatch(hostname, port) :: remain =>
          parseOptions_(remain, currentOption.copy(httpHost = hostname, httpPort = port.toInt))
        case "--hadoop" :: addrMatch(hostname, port) :: remain =>
          parseOptions_(remain, currentOption.copy(hadoopHost = hostname, hadoopPort = port.toInt))
        case "--spark" :: addrMatch(hostname, port) :: remain =>
          parseOptions_(remain, currentOption.copy(sparkHost = hostname, sparkPort= port.toInt))
        case "--cluster" :: addrMatch(hostname, port) :: remain =>
          parseOptions_(remain, currentOption.copy(clusterHost = hostname, clusterPort = port.toInt))
        case "--tile" :: addrMatch(hostname, port) :: remain =>
          parseOptions_(remain, currentOption.copy(tileHost = hostname, tilePort = port.toInt))
        case "--imdb" :: addrMatch(hostname, port) :: remain =>
          parseOptions_(remain, currentOption.copy(imdbHost = hostname, imdbPort = port.toInt))
        case _ => currentOption
      }
    }

    parseOptions_(args, basicOption)
  }

  def main(args: Array[String]): Unit = {
    implicit val config: SMUDIConfigOptions = ConfigFactory.load()

    val options = parseOptions(args.toList)

    val pw = new PrintWriter(new File("application.conf"))

    pw.write("\"smudi\": ")
    pw.write(options.root.get("smudi").render())

    pw.close()

    println("Writing configuration complete.")
  }
}
