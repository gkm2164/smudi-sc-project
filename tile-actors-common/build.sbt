import Dependencies._

name := "tile-actors-common"

libraryDependencies ++= Seq(
  spark,
  geotrellis,
  `akka-actor`
)