package com.satreci.smudi.image

import java.net.URI
import java.util.concurrent.ConcurrentHashMap

import com.satreci.smudi.hdfs.HdfsImageFS
import com.satreci.smudi.messages.RequestQuery
import com.satreci.smudi.tile.entity.{NullTileExtent, TileExtent}
import com.typesafe.config.Config
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

class ImageInventory(implicit config: Config) {
  val hdfsHost: String = config.getString("smudi.hadoop.address")

  case class EntityRange[A](min: A, max: A)(implicit ordering: A => Ordered[A]) {
    def isInRange(num: A): Boolean = (num >= min) && (num <= max)
  }

  case class ImageEntry(row: EntityRange[Int], col: EntityRange[Int])

  case class ImageCatalogEntry(catalog: String) {
    private val ce: Map[Int, ImageEntry] = imageInfo(catalog)
    val level = EntityRange(ce.keys.min, ce.keys.max)
    def apply(level: Int): ImageEntry = ce(level)
    def contains(level: Int): Boolean = ce contains level

    def imageInfo(catalog: String): Map[Int, ImageEntry] = {
      import com.satreci.smudi.tile.entity.TileExtentJsonProtocol._
      import spray.json._

      val conf = new Configuration()
      val hdfs = FileSystem.get(new URI(hdfsHost), conf)

      val jsonInputStream = hdfs.open(new Path(HdfsImageFS.tileOrigMetaPath(catalog)))

      val ss = new StringBuffer()

      val bytes = Array.ofDim[Byte](jsonInputStream.getWrappedStream.available())

      val ba = jsonInputStream.read(bytes)

      val sb = StringBuilder.newBuilder.appendAll(bytes.map(_.toChar)).toString

      val seed = JsonParser(sb).convertTo[TileExtent]

      def currentEntry(current: TileExtent, result: Map[Int, ImageEntry]): Map[Int, ImageEntry] = {
        if (current == NullTileExtent)
          result
        else {
          val newmap = result + (current.level -> ImageEntry(row = EntityRange(current.rmin, current.rmax), col = EntityRange(current.cmin, current.cmax)))
          currentEntry(current.prevLevelExtents, newmap)
        }
      }

      val ret = currentEntry(seed, Map())
      println(catalog, ret)
      ret
    }
  }

  def getCatalogs: Array[String] = {
    val conf = new Configuration()
    val hdfs = FileSystem.get(new URI(hdfsHost), conf)

    val ls = hdfs.listStatus(new Path(HdfsImageFS.origPath))

    val ret = ls.filter(_.getPath.getName.contains(".json")).map(_.getPath.getName.replace(".json", ""))

    ret.foreach(println)

    ret
  }

  val _images: ConcurrentHashMap[String, ImageCatalogEntry] = new ConcurrentHashMap[String, ImageCatalogEntry]

  //val images: mutable.Map[String, ImageCatalogEntry] = {
    //mutable.Map(getCatalogs.map(catalog => catalog -> ImageCatalogEntry(catalog)).toMap)
  //}

  import scala.collection.JavaConverters._

  def images = _images.asScala

  def isValidQuery(rq: RequestQuery): Boolean =
    (_images containsKey rq.catalog) &&
      (_images.get(rq.catalog) contains rq.level) &&
      _images.get(rq.catalog)(rq.level).row.isInRange(rq.row) &&
      _images.get(rq.catalog)(rq.level).col.isInRange(rq.col)

  def tileExist(rq: RequestQuery): Boolean = {
    val conf = new Configuration()
    val hdfs = FileSystem.get(new URI(hdfsHost), conf)
    val filePath = rq match {
      case RequestQuery(catalog, level, row, col) =>
        new Path(HdfsImageFS.tilePath(catalog, level, row, col))
    }
    hdfs.exists(filePath)
  }

  def loadImage(catalog: String) = {
    _images.put(catalog, ImageCatalogEntry(catalog))
  }

  getCatalogs.foreach(loadImage)
}
