package com.satreci.smudi.messages

sealed trait ImageQuery
case class Query(query: String) extends ImageQuery
case class RequestQuery(catalog: String, level: Int, row: Int, col: Int)

sealed trait TileSparkOperation
case class TileCreateRequest(level: Int, row: Int, col: Int) extends TileSparkOperation
case class TileStateResult(level: Int, row: Int, col: Int, tileState: TileState) extends TileSparkOperation
case class MergeTile(level: Int, row: Int, col: Int) extends TileSparkOperation
case class WaitSingleTile(level: Int, row: Int, col: Int, callback: (TileState) => Unit) extends TileSparkOperation
case class DispatchWaitQueue(level: Int, row: Int, col: Int) extends TileSparkOperation

sealed trait CacheState

case object CacheState {
  case object Invalid extends CacheState
  case object Preparing extends CacheState
  case object Valid extends CacheState
}

sealed trait TileCacheInventoryOperation
case object InvalidateAll extends TileCacheInventoryOperation
case class InvalidateCache(level: Int, row: Int, col: Int) extends TileCacheInventoryOperation
case class SetCacheState(level: Int, row: Int, col: Int, cacheState: CacheState) extends TileCacheInventoryOperation
case class GetCacheState(level: Int, row: Int, col: Int) extends TileCacheInventoryOperation

sealed trait TileState

case object TileState {
  case object Unknown extends TileState
  case object Preparing extends TileState
  case object Available extends TileState
  case object Unavailable extends TileState
}

object TileStatus {
  sealed trait TileOperation
  case object ClearAll extends TileOperation
  case class Clear(level: Int, row: Int, col: Int) extends TileOperation
  case class PrepareTile(level: Int, row: Int, col: Int) extends TileOperation
  case class GetTileState(level: Int, row: Int, col: Int) extends TileOperation
  case class SetTileState(level: Int, row: Int, col: Int, tileState: TileState) extends TileOperation
  case class RefreshTileState(level: Int, row: Int, col: Int) extends TileOperation
  case class WaitTileResult(level: Int, row: Int, col: Int, callback: (TileState) => Unit) extends TileOperation
  case class WaitAvailabilityCluster(catalog: String, level: Int, row: Int, col: Int) extends TileOperation
  case class DispatchQueue(level: Int, row: Int, col: Int) extends TileOperation
  case object DispatchAll extends TileOperation

}

sealed trait TileCacheOperation
case object CacheClearAll extends TileCacheOperation
case class InvalidateCacheEntry(level: Int, row: Int, col: Int) extends TileCacheOperation
case class LoadCacheEntry(level: Int, row: Int, col: Int) extends TileCacheOperation
case class LoadCacheEntryFire(level: Int, row: Int, col: Int) extends TileCacheOperation

sealed trait SystemOperations
case object ShowWaitQueue extends SystemOperations
case class LoadCatalog(catalog: String) extends SystemOperations