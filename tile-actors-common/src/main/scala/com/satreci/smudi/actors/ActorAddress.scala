package com.satreci.smudi.actors

object ActorAddress {
  def TileSpark(catalog: String) = s"/user/${catalog}TileSpark"
  def TileInventory(catalog: String) = s"/user/${catalog}TileInventory"
  def TileCache(catalog: String) = s"/user/${catalog}TileCache"
  def TileCacheInventory(catalog: String) = s"/user/${catalog}TileCacheInventory"
}
