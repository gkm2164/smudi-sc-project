package com.satreci.smudi.mailbox

import akka.actor.{ActorRef, ActorSystem}
import akka.dispatch.{MessageQueue, ProducesMessageQueue}
import com.typesafe.config.Config

class MonitorMailboxType(settings: ActorSystem.Settings, config: Config)
  extends akka.dispatch.MailboxType
    with ProducesMessageQueue[MonitorQueue] {
  override def create(owner: Option[ActorRef], system: Option[ActorSystem]): MessageQueue = {
    system match {
      case Some(sys) => new MonitorQueue(sys)
        new MonitorQueue(sys)
      case _ => throw new IllegalArgumentException("requires a system")
    }
  }
}
