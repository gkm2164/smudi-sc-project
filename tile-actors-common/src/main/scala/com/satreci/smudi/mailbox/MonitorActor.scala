package com.satreci.smudi.mailbox

import akka.actor.{Actor, ActorLogging}

case class ActorStatistics(receiver: String, sender: String, entryTime: Long, exitTime: Long)

trait MonitorActor extends Actor with ActorLogging {
  abstract override def receive = {
    case m: Any => {
      val start = System.currentTimeMillis()
      super.receive(m)
      val end = System.currentTimeMillis()

      val stat = ActorStatistics(
        self.toString(),
        sender.toString(),
        start,
        end
      )

      println(s"$stat for $m")
      context.system.eventStream.publish(stat)
    }
  }
}
