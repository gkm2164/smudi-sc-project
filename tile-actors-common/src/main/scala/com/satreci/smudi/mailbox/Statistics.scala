package com.satreci.smudi.mailbox

import java.util.concurrent.ConcurrentLinkedQueue

import akka.actor.{ActorRef, ActorSystem}
import akka.dispatch.{Envelope, MessageQueue, UnboundedMessageQueueSemantics}
import akka.event.LoggerMessageQueueSemantics

case class MonitorEnvelope(queueSize: Int, receiver: String, entryTime: Long, handle: Envelope)

case class MailboxStatistics(queueSize: Int, receiver: String, sender: String, entryTime: Long, exitTime: Long)

class MonitorQueue(val system: ActorSystem)
  extends MessageQueue
    with UnboundedMessageQueueSemantics
    with LoggerMessageQueueSemantics {
  private final val queue = new ConcurrentLinkedQueue[MonitorEnvelope]()

  override def enqueue(receiver: ActorRef, handle: Envelope): Unit = {
    val env = MonitorEnvelope(queueSize = queue.size() + 1,
      receiver = receiver.toString(),
      entryTime = System.currentTimeMillis(),
      handle = handle)
    queue add env
  }

  override def dequeue(): Envelope = {
    val monitor = queue.poll()
    if (monitor != null) {
      monitor.handle.message match {
        case _: MailboxStatistics =>
        case x => {
          val stat = MailboxStatistics(
            queueSize = monitor.queueSize,
            receiver = monitor.receiver,
            sender = monitor.handle.sender.toString,
            entryTime = monitor.entryTime,
            exitTime = System.currentTimeMillis
          )

          system.eventStream.publish(stat)
        }
      }
      monitor.handle
    } else {
      null
    }
  }

  override def numberOfMessages: Int = queue.size

  override def hasMessages: Boolean = !queue.isEmpty


  override def cleanUp(owner: ActorRef, deadLetters: MessageQueue): Unit = {
    if (hasMessages) {
      var envelope = dequeue
      while(envelope ne null) {
        deadLetters.enqueue(owner, envelope)
        envelope = dequeue
      }
    }
  }
}