package com.satreci.smudi.hdfs

import com.satreci.smudi.tile.entity.TileCoord
import org.apache.hadoop.fs.{FileSystem, Path}

object HdfsImageFS {
  val origPath = "/image/orig"

  val divFactor = 32
  def div(num: Int, divider: Int = divFactor): Int = num - num % divider
  def folderIdx(row: Int, col: Int): String = s"${div(row, divFactor)}_${div(col, divFactor)}"
  def tileDir(catalog: String, level: Int, row: Int, col: Int, imgType: String): String =
    s"/image/$imgType/$catalog/$level/${folderIdx(row, col)}"
  def tileNamePath(catalog: String, level: Int, row: Int, col: Int, imgType: String): String =
    s"${tileDir(catalog, level, row, col, imgType)}/${catalog}_${level}_${col}_$row"
  def tileCatalogPath(catalog: String): String =
    s"$origPath/$catalog"

  // TIFF Image
  def tilePath(catalog: String, level: Int, row: Int, col: Int): String =
    tileNamePath(catalog, level, row, col, "tile") + ".tif"
  // DDS Image
  def tileCachePath(catalog: String, level: Int, row: Int, col: Int): String =
    tileNamePath(catalog, level, row, col, "cache") + ".dds"


  def tileOrigPath(catalog: String): String =
    s"${tileCatalogPath(catalog)}.tif"
  def tileOrigMetaPath(catalog: String): String =
    s"${tileCatalogPath(catalog)}.json"

  def tileDirExist(catalog: String, tc: TileCoord, imgType: String)(implicit hdfs: FileSystem): Boolean =
    hdfs.exists(new Path(tileDir(catalog, tc.level, tc.row, tc.col, imgType)))
}
