import sbt._
import Versions._

object Dependencies {
  val geotrellis          = "org.locationtech.geotrellis" %% "geotrellis-spark" % "1.2.0-SNAPSHOT"
  val spark               = "org.apache.spark" %% "spark-core" % "2.1.0"
  val hadoop              = "org.apache.hadoop" % "hadoop-common" % "2.2.0"
  val jodatime            = "joda-time" % "joda-time" % "2.9.9"
  val jodaconv            = "org.joda" % "joda-convert" % "1.8.1"
  val `akka-actor`        = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val `akka-persistence`  = "com.typesafe.akka" %% "akka-persistence" % akkaVersion
  val `akka-http`         = "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
  val `akka-cluster`      = "com.typesafe.akka" %% "akka-cluster" % akkaVersion
  val `akka-cluster-tool` = "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion
  val `typesafe-config`   = "com.typesafe" % "config" % "1.3.1"
  val leveldb             = "org.iq80.leveldb"            % "leveldb"          % "0.7"
  val `leveldbjni-all`    = "org.fusesource.leveldbjni"   % "leveldbjni-all"   % "1.8"
  val `akka-persistence-redis` = "com.hootsuite" %% "akka-persistence-redis" % "0.8.0"
  val `rediscala`         = "com.github.etaty" %% "rediscala" % "1.8.0"
  val `spray-json`        = "io.spray" %%  "spray-json" % "1.3.3"
  val scalatic            = "org.scalactic" %% "scalactic" % "3.0.1"
  val scalatest           = "org.scalatest" %% "scalatest" % "3.0.1" % "test"
  //noinspection TypeAnnotation
  implicit def toSparkDep(mid: ModuleID) = new {
    def toSparkDependency(attachScalaLangVer: Boolean = true): String = {
      val org = mid.organization
      val art = mid.name
      val ver = mid.revision
      val art_postfix = if (attachScalaLangVer) s"_$scalaLangVer" else ""
      s"$org:$art$art_postfix:$ver"
    }
  }
}