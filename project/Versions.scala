object Versions {
  val tileServerVersion = "0.93"
  val scalaLangVer = "2.11"
  val smudiScalaVersion = "2.11.8"
  val akkaVersion = "2.4.19"
  val akkaHttpVersion = "10.0.10"
}
