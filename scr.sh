#!/bin/sh
tmux new-session -d -s land-tile 'sbt "project tile-uploader" "run tif /home/gyeongmin/maps/land.tif land"'
tmux new-session -d -s k3img-tile 'sbt "project tile-uploader" "run tif /home/gyeongmin/maps/k3img.tif k3img"'
tmux new-session -d -s terrasar-tile 'sbt "project tile-uploader" "run tif /home/gyeongmin/maps/terrasar_4band.tif terrasar"'
