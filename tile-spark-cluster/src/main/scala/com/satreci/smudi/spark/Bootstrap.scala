package com.satreci.smudi.spark

import java.io.ByteArrayInputStream

import akka.actor._
import akka.cluster.client._
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.MediaType.Compressible
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.settings._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.StreamConverters
import akka.util.Timeout
import com.satreci.smudi.Bootstrap.config
import com.satreci.smudi.actors.ActorAddress._
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.mailbox.MonitorActor
import com.satreci.smudi.messages.TileStatus._
import com.satreci.smudi.messages._
import com.satreci.smudi.tile.entity.TileCoord
import com.typesafe.config.ConfigFactory
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.writer.GeoTiffWriter

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.io.StdIn
import scala.util.{Failure, Success}

object Bootstrap extends App {
  implicit val config = ConfigFactory.load()

  implicit val system: ActorSystem = ActorSystem.create("smudi-clusters")

  val clusterListener = system.actorOf(Props(new ClusterListener with MonitorActor), "cluster-listener")

  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val requestTimeout: Timeout = akka.util.Timeout(10 seconds)

  implicit val imageInventory: ImageInventory = new ImageInventory

  val seedAddr = Address(
    config.getString("smudi.cluster-seed.protocol"),
    config.getString("smudi.cluster-seed.name"),
    config.getString("smudi.cluster-seed.host"),
    config.getInt("smudi.cluster-seed.port")
  )

  println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + seedAddr.toString)

  val initialContacts = Set(
    ActorPath.fromString(s"${seedAddr.toString}/system/receptionist")
  )

  val c = system.actorOf(
    ClusterClient.props(
      ClusterClientSettings(system).withInitialContacts(initialContacts)
    ), "client")


  val `image/x-dds`: MediaType.Binary = MediaType.customBinary("image", "x-dds", Compressible, List("dds"))

  val route: Route =
    path("tile" / "make" / "all") {
      get {
        parameter("catalog".as[String] ? "land") { (catalog) =>
          if (!(imageInventory.images contains catalog)) {
            complete(StatusCodes.BadRequest)
          } else {
            val levels = imageInventory.images(catalog).level
            val level = levels.min
            for {
              //level <- levels.max to levels.min by -1
              row <- imageInventory.images(catalog)(level).row.min until imageInventory.images(catalog)(level).row.max
              col <- imageInventory.images(catalog)(level).col.min until imageInventory.images(catalog)(level).col.max
            } yield {
              println(s"Request: ${TileCreateRequest(level, row, col)}")
              c ! ClusterClient.Send(TileSpark(catalog), TileCreateRequest(level, row, col), localAffinity = false)
            }

            complete(StatusCodes.OK)
          }
        }
      }
    } ~ path("tile" / "make") {
      get {
        parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
          if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {
            c ! ClusterClient.Send(TileSpark(catalog), TileCreateRequest(level, row, col), localAffinity = false)
            complete(StatusCodes.OK)
          } else {
            complete(StatusCodes.BadRequest)
          }
        }
      }
    } ~ path("tile" / "tif") {
      get {
        parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
          if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {

            val ret: Future[Any] = c ? ClusterClient.Send(TileSpark(catalog), TileCoord(level, row, col), localAffinity = false)

            onComplete(ret) {
              case Success(Some(tiff: MultibandGeoTiff)) =>
                val ab = GeoTiffWriter.write(tiff)
                val bais = new ByteArrayInputStream(ab)
                val stream = StreamConverters.fromInputStream(() => bais)
                complete(HttpEntity(ContentType(MediaTypes.`image/tiff`), ab.length, stream))
              case Success(_) => complete(StatusCodes.ExpectationFailed)
              case Failure(whatisit) =>
                println(whatisit)
                complete(StatusCodes.BadRequest)
              case _ =>
                println("Unknown system state")
                complete(StatusCodes.BadRequest)
            }
          } else {
            complete(StatusCodes.BadRequest)
          }
        }
      }
    } ~ path("tile" / "png") {
        get {
          parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
            if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {

              val ret: Future[Any] = c ? ClusterClient.Send(TileSpark(catalog), TileCoord(level, row, col), localAffinity = false)
              onComplete(ret) {
                case Success(Some(tiff: MultibandGeoTiff)) =>
                  println(s"Tile bands: ${tiff.tile.bandCount}")
                  val pngBytes = tiff.tile.resample(512, 512).renderPng.bytes
                  val bais = new ByteArrayInputStream(pngBytes)
                  val stream = StreamConverters.fromInputStream(() => bais)
                  complete(HttpEntity(ContentType(MediaTypes.`image/png`), pngBytes.length, stream))
                case Success(_) => complete(StatusCodes.ExpectationFailed)
                case Failure(whatisit) =>
                  println(whatisit)
                  complete(StatusCodes.BadRequest)
                case _ =>
                  println("Unknown system state")
                  complete(StatusCodes.BadRequest)
              }
            } else {
              complete(StatusCodes.BadRequest)
            }
          }
        }
      } ~ path("tile" / "state") {
        get {
          parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
            if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {

              val result = c ? ClusterClient.Send(TileInventory(catalog), GetTileState(level, row, col), localAffinity = false)
              onSuccess(result) {
                case x: TileState => complete(HttpEntity(x.toString))
                case _ => complete(StatusCodes.NoContent)
              }
            } else {
              complete(StatusCodes.BadRequest)
            }
          }
        }
      } ~ path("tile" / "cache" / "all") {
        get {
          parameter("catalog".as[String] ? "land") { (catalog) =>
            if (!(imageInventory.images contains catalog)){
              complete(StatusCodes.BadRequest)
            } else {
              val levels = imageInventory.images(catalog).level
              for {
                level <- levels.min to levels.max
                row <- imageInventory.images(catalog)(level).row.min until imageInventory.images(catalog)(level).row.max
                col <- imageInventory.images(catalog)(level).col.min until imageInventory.images(catalog)(level).col.max
              } yield {
                c ! ClusterClient.Send(TileCache(catalog), LoadCacheEntryFire(level, row, col), localAffinity = false)
              }

              complete(StatusCodes.OK)
            }
          }
        }
      } ~ path("tile" / "cache" / "create") {
        get {
          parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
            if (!(imageInventory.images contains catalog)) {
              complete(StatusCodes.BadRequest)
            } else {
              c ! ClusterClient.Send(TileCache(catalog), LoadCacheEntryFire(level, row, col), localAffinity = false)
              complete(StatusCodes.OK)
            }
          }
        }
      } ~ path("tile" / "cache" / "invalidate") {
        get {
          parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
            if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {
              c ! ClusterClient.Send(TileCache(catalog), InvalidateCacheEntry(level, row, col), localAffinity = false)

              complete(StatusCodes.OK)
            } else {
              complete(StatusCodes.BadRequest)
            }
          }
        }
      } ~ path("tile" / "cache" / "empty") {
        get {
          parameter("catalog".as[String] ? "land") { (catalog) =>
            c ! ClusterClient.Send(TileCache(catalog), CacheClearAll, localAffinity = false)
            complete(StatusCodes.OK)
          }
        }
      } ~ path("tile" / "cache" / "state") {
      get {
        parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") {
          (level, row, col, catalog) =>
            if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {
              val result = c ? ClusterClient.Send(TileCacheInventory(catalog), GetCacheState(level, row, col), localAffinity = false)

              onComplete(result) {
                case Success(cs: CacheState) => complete(HttpEntity(cs.toString))
                case _ => complete(StatusCodes.NoContent)
              }
            } else {
              complete(StatusCodes.BadRequest)
            }
        }
      }
    } ~ path("tile" / "cache") {
      get {
        parameter("level".as[Int], "row".as[Int], "col".as[Int], "catalog".as[String] ? "land") { (level, row, col, catalog) =>
          if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {
            val tileStateResult = c ? ClusterClient.Send(TileInventory(catalog), GetTileState(level, row, col), localAffinity = false)

            onComplete(tileStateResult) {
              case Success(_) =>
                val result = c ? ClusterClient.Send(TileCache(catalog), LoadCacheEntry(level, row, col), localAffinity = false)

                onComplete(result) {
                  case Success(ab: Array[Byte]) =>
                    val is = new ByteArrayInputStream(ab)
                    val stream = StreamConverters.fromInputStream(() => is)
                    complete(HttpEntity(ContentType(`image/x-dds`), ab.length, stream))
                  case _ => complete(StatusCodes.BadRequest)
                }
              case _ => complete(StatusCodes.BadRequest)
            }
          } else {
            complete(StatusCodes.BadRequest)
          }
        }
      }
    } ~ path("sys" / "waitqueues") {
      get {
        parameter("catalog".as[String] ? "land") { catalog =>
          val result = c ? ClusterClient.Send(TileInventory(catalog), ShowWaitQueue, localAffinity = false)

          onComplete(result) {
            case Success(waitQueueList: String) => complete(HttpEntity(waitQueueList.toString))
            case _ => complete(StatusCodes.NoContent)
          }
        }
      }
    } ~ path("sys" / "loadmap") {
      get {
        parameter("catalog".as[String] ? "land") { catalog =>
          imageInventory.loadImage(catalog)

          c ! ClusterClient.SendToAll("SparkLoader", LoadCatalog(catalog))
          c ! ClusterClient.SendToAll("CacheLoader", LoadCatalog(catalog))

          complete(StatusCodes.NoContent)
        }
      }
    } ~ path("sys" / "maplist") {
      get {
        complete(imageInventory.images.toString)
      }
    }

  val httpHost = config.getString("smudi.http.hostname")
  val port = config.getInt("smudi.http.port")

  val parserSettings = ParserSettings(system).withCustomMediaTypes(`image/x-dds`)
  val serverSettings = ServerSettings(system).withParserSettings(parserSettings)

  val bindingFuture = Http().bindAndHandle(route, httpHost, port = port, settings = serverSettings)

  println(s"Server online at http://0.0.0.0:$port/\nPress RETURN to stop...")

  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
