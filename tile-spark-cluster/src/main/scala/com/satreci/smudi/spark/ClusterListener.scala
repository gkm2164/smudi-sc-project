package com.satreci.smudi.spark

import akka.actor.{Actor, ActorLogging}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent._

class ClusterListener extends Actor with ActorLogging {
  val cluster = Cluster(context.system)

  override def preStart(): Unit = {
    cluster.subscribe(self, initialStateMode = InitialStateAsEvents,
      classOf[MemberEvent], classOf[UnreachableMember])
  }
  override def postStop(): Unit = cluster.unsubscribe(self)


  override def receive: Receive = {
    case MemberUp(member) => log.info(s"[ClusterListener] $member joined")
    case UnreachableMember(member) => log.info(s"[ClusterListener] $member is unreachable")
    case MemberRemoved(member, previousStatus) => log.info(s"[ClusterListener] previously $previousStatus $member has been removed")

    case _: MemberEvent => // ignore
  }
}