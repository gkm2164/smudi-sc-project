import Dependencies._

name := "tile-spark-cluster"

libraryDependencies ++= Seq(
  geotrellis,
  spark,
  `akka-cluster`,
  `akka-http`,
  `akka-cluster-tool`
)