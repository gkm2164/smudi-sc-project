import sbt.IO
import sbt.complete._

lazy val commonSettings = Seq(
  organization := "com.satreci.smudi",
  scalaVersion := Versions.smudiScalaVersion,
  version := Versions.tileServerVersion,
  shellPrompt := { s => Project.extract(s).currentProject.id + " > " },
  scalacOptions := Seq("-feature", "-language:implicitConversions", "-language:reflectiveCalls", "-language:postfixOps")
  )

lazy val `smudi-sc-project` = (project in file("."))
  .aggregate(`coord-system`,
    `tile-actors-common`, `geo-setter`,
    `tile-spark`, `tile-spark-cluster`, `tile-state-persistence`,
    `tile-uploader`, `map-cutter`, `config-generator`)
  .disablePlugins(SparkSubmitPlugin)
  .settings(commonSettings: _*)

lazy val `tile-actors-common` = (project in file("./tile-actors-common"))
  .settings(commonSettings)
  .dependsOn(`coord-system`)

lazy val `geo-setter` = (project in file("./geo-setter"))
  .settings(commonSettings)
  .dependsOn(`coord-system`)

lazy val `tile-state-persistence` = (project in file("./tile-state-persistence"))
  .settings(commonSettings)
  .dependsOn(`coord-system`, `tile-actors-common`)

lazy val `tile-spark` = (project in file("./tile-spark"))
  .settings(commonSettings,
    SparkSubmitSetting(
      SparkSubmitSetting("spark2552",
        Seq(), Seq("-Dakka.remote.netty.tcp.port=2552")),
      SparkSubmitSetting("spark2553",
        Seq(), Seq("-Dakka.remote.netty.tcp.port=2553")),
      SparkSubmitSetting("spark2554",
        Seq(), Seq("-Dakka.remote.netty.tcp.port=2554"))
    )
  )
  .enablePlugins(SparkSubmitPlugin)
  .dependsOn(`coord-system`, `tile-state-persistence`, `tile-actors-common`)

lazy val `tile-spark-cluster` = (project in file("./tile-spark-cluster"))
  .settings(commonSettings)
  .dependsOn(`tile-spark`, `coord-system`, `tile-state-persistence`, `tile-actors-common`)

lazy val `tile-uploader` = (project in file("./tile-uploader"))
  .settings(commonSettings)
  .dependsOn(`coord-system`, `tile-actors-common`)

lazy val `coord-system` = (project in file("./coord-system"))
  .settings(commonSettings)

lazy val `map-cutter` = (project in file("./map-cutter"))
  .settings(commonSettings)
  .dependsOn(`coord-system`)

lazy val `config-generator` = (project in file("./config-generator"))
  .settings(commonSettings)

lazy val makeConfig = inputKey[Unit]("Make configuration files")

makeConfig := {
  val config = file(".")

  val customConf = IO.read(config / "application.conf")

  val commonOutdir = file("tile-actors-common") / "src"/ "main" / "resources"

  IO.write(commonOutdir / "application.conf", customConf)
}