package com.satreci.smudi.tile.io.writer
import java.io.{File, FileOutputStream, OutputStream}

import com.satreci.smudi.tile.entity.TileCoord

class GeneralFileWriter extends TileWriter {
  override def skipWrite(catalog: String, tileCoord: TileCoord) = false
  override def write(catalog: String, tileCoord: TileCoord, bytes: Array[Byte]): Unit = {
    val (level, row, col) = tileCoord match {
      case TileCoord(l, r, c) => (l, r, c)
    }
    val fos = new FileOutputStream(new File(s"./${catalog}_${level}_${col}_${row}.tif"))

    fos.write(bytes)
    fos.close()
  }

  override def write(catalog: String, tileCoord: TileCoord, writerDelegate: OutputStream => Unit): Unit = {
    val (level, row, col) = tileCoord match {
      case TileCoord(l, r, c) => (l, r, c)
    }

    val fos = new FileOutputStream(new File(s"./${catalog}_${level}_${col}_${row}.tif"))

    writerDelegate(fos)
    fos.close()
  }
}
