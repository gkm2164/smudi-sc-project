package com.satreci.smudi.tile.io.cropper

import com.satreci.smudi.tile.entity.{TileCoord, TileExtent, TileResolution}
import com.satreci.smudi.tile.io.ImageRangeReader
import com.satreci.smudi.tile.io.writer.TileWriter
import com.satreci.smudi.{CoordSystemDesc, CoordToTileUtil}
import geotrellis.proj4.LatLng
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.writer.GeoTiffWriter
import geotrellis.raster.{TileExtents, TileLayout}
import geotrellis.util.StreamingByteReader
import geotrellis.vector.{Extent, ProjectedExtent}

class GeoTiffCropper(tiffReader: MultibandGeoTiff, catalog: String, catalogWriter: TileExtent => Unit) extends ImageCropper {
  assert(tiffReader.tile.bandCount >= 3)
  val pext: Extent = tiffReader.projectedExtent.reproject(LatLng)

  val width = pext.xmax - pext.xmin
  val height = pext.ymax - pext.ymin

  val colSize = width / tiffReader.tile.cols
  val rowSize = height / tiffReader.tile.rows

  val level = CoordToTileUtil.findNearestLevel(Math.min(colSize, rowSize))

  val lrc: TileResolution = CoordSystemDesc.levelRowCol(level)

  val tileLayout = TileLayout(lrc.cols.toInt, lrc.rows.toInt, 512, 512)
  val extent = Extent(-180, -90, 180, 90)
  val tileExtents = TileExtents(extent, tileLayout)
  val teOrig = TileExtent(tiffReader)

  catalogWriter(teOrig)

  override def crop(output: TileWriter, col: Int, row: Int, targetWidth: Int, targetHeight: Int): Unit = {
    if (output.skipWrite(catalog, TileCoord(level, row, col)))
      return

    val pe = ProjectedExtent(tileExtents(col, row), LatLng)
    val newTile = tiffReader.crop(pe.reproject(tiffReader.crs)).tile.resample(targetWidth, targetHeight)

    val newTiff = MultibandGeoTiff(newTile, pe.extent, LatLng)
    val byteArr = GeoTiffWriter.write(newTiff)
    println(s"$level, $row, $col, Bandcount: ${newTile.bandCount}")

    assert(newTiff.tile.bandCount >= 3)

    output.write(catalog, TileCoord(level, row, col), byteArr)
  }

  override def rowRange = teOrig.rmin until teOrig.rmax
  override def colRange = teOrig.cmin until teOrig.cmax
}

object GeoTiffCropper {
  def apply(tiffPath: String, catalog: String, catalogWriter: TileExtent => Unit): GeoTiffCropper =
    GeoTiffCropper(MultibandGeoTiff.streaming(StreamingByteReader(ImageRangeReader(tiffPath), 65536)), catalog, catalogWriter)
  def apply(tiff: MultibandGeoTiff, catalog: String, catalogWriter: TileExtent => Unit) =
    new GeoTiffCropper(tiff, catalog, catalogWriter)
}
