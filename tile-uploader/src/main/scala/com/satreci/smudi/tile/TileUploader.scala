package com.satreci.smudi.tile

import java.net.URI

import com.satreci.smudi.hdfs.HdfsImageFS
import com.satreci.smudi.tile.entity.{NullTileExtent, TileExtent}
import com.satreci.smudi.tile.io.ImageRangeReader
import com.satreci.smudi.tile.io.cropper.{CombinedGeoTiffCropper, GeoTiffCropper, JP2Cropper, JPEGCropper}
import com.satreci.smudi.tile.io.writer.{GeneralFileWriter, HadoopTileWriter}
import com.typesafe.config.ConfigFactory
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.util.StreamingByteReader
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import spray.json.{DefaultJsonProtocol, JsNumber, JsObject, JsValue, RootJsonFormat}

import scala.collection.GenSeq

object TileUploader {
  implicit val config = ConfigFactory.load()

  val hdfsHost = config.getString("smudi.hadoop.address")

  object TileExtentJsonProtocol extends DefaultJsonProtocol {

    implicit object TileExtentJsonFormat extends RootJsonFormat[TileExtent] {
      override def write(obj: TileExtent) =
        JsObject(
          "level" -> JsNumber(obj.level),
          "rmin" -> JsNumber(obj.rmin),
          "cmin" -> JsNumber(obj.cmin),
          "rmax" -> JsNumber(obj.rmax),
          "cmax" -> JsNumber(obj.cmax)
        )


      override def read(json: JsValue): TileExtent =
        json.asJsObject.getFields("level", "rmin", "cmin", "rmax", "cmax") match {
          case Seq(JsNumber(level), JsNumber(cmin), JsNumber(rmin), JsNumber(cmax), JsNumber(rmax)) =>
            TileExtent(level.toInt, rmin.toInt, cmin.toInt, rmax.toInt, cmax.toInt)
          case _ => NullTileExtent
        }
    }

  }

  def makeCatalog(catalog: String, tileExtent: TileExtent): Unit = {
    val outputName = HdfsImageFS.tileOrigMetaPath(catalog.replace("_", ""))

    val conf = new Configuration()
    val hdfs = FileSystem.get(new URI(hdfsHost), conf)

    import TileExtentJsonProtocol._
    import spray.json._

    val jsonAst: JsValue = tileExtent.toJson

    val fileStream = hdfs.create(new Path(outputName))
    fileStream.writeBytes(jsonAst.toString())
    fileStream.close()
  }

  def main(args: Array[String]): Unit = {
    if (args.length != 3) {
      println("Usage: tile-uploader [type] [image] [catalog-name]")
      println("\tExample: tile-uploader ./land.tif land")
      return
    }

    val fileType = args(0)
    val localFileName = args(1)
    val catalogName = args(2)

    val seqs: GenSeq[Int] with Immutable = if (true) (1 to 3).par else (1 to 3) ++ (2 to 5)

    /* Making directory */

    val (tileWriter, cropper) =
      fileType match {
      case "tiffs" =>
        (new HadoopTileWriter, CombinedGeoTiffCropper(localFileName, catalogName, (te: TileExtent) => makeCatalog(catalogName, te)))
      case "tif" | "tiff" =>
        val tiff = MultibandGeoTiff.streaming(StreamingByteReader(ImageRangeReader(localFileName), 65536))
        (new HadoopTileWriter, GeoTiffCropper(tiff, catalogName, (te: TileExtent) => makeCatalog(catalogName, te)))
      case "jp2" =>
        (new HadoopTileWriter, JP2Cropper(localFileName, catalogName, (te: TileExtent) => makeCatalog(catalogName, te)))
      case "jpg" =>
        (new HadoopTileWriter, JPEGCropper(localFileName, catalogName, (te: TileExtent) => makeCatalog(catalogName, te)))
      case "tjp2" =>
        (new GeneralFileWriter, JP2Cropper(localFileName, catalogName, (te: TileExtent) => makeCatalog(catalogName, te)))
    }

    /* Tiling maximum level of tiff */
    for {
      row <- cropper.rowRange.par
      col <- cropper.colRange.par
    } yield {
      val time = System.currentTimeMillis()

      def tryCrop(remainCnt: Int): Unit = {
        if (remainCnt > 0) {
          println(s"Trying $remainCnt times")
          try {
            cropper.crop(tileWriter, col, row, 512, 512)
          } catch {
            case e: Exception => println(s"There was an error on ${(row, col)}")
            e.printStackTrace()
            tryCrop(remainCnt - 1)
          }
        }
      }
      
      tryCrop(3)
      val timeElapsed = System.currentTimeMillis() - time
      println(s"${timeElapsed}ms elapsed for processing ${row}, ${col} of ${catalogName}")
    }

  }
}
