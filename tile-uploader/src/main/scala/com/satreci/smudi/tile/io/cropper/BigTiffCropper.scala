package com.satreci.smudi.tile.io.cropper
import com.satreci.smudi.{CoordSystemDesc, CoordToTileUtil}
import com.satreci.smudi.tile.entity.{TileCoord, TileExtent, TileResolution}
import com.satreci.smudi.tile.io.writer.TileWriter
import geotrellis.proj4.LatLng
import geotrellis.raster.{TileExtents, TileLayout}
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.writer.GeoTiffWriter
import geotrellis.vector.{Extent, ProjectedExtent}

import scala.collection.GenSeq

class BigTiffCropper(tiffReader: MultibandGeoTiff, catalog: String, cachingLevel: Int = 10) extends ImageCropper {
  val thisExtent = TileExtent(tiffReader)

  override def rowRange: GenSeq[Int] = thisExtent.rmin to thisExtent.rmax

  override def colRange: GenSeq[Int] = thisExtent.cmin to thisExtent.cmax

  val pext: Extent = tiffReader.projectedExtent.reproject(LatLng)

  val width = pext.xmax - pext.xmin
  val height = pext.ymax - pext.ymin

  val colSize = width / tiffReader.tile.cols
  val rowSize = height / tiffReader.tile.rows

  val level = CoordToTileUtil.findNearestLevel(Math.min(colSize, rowSize))
  val lrc: TileResolution = CoordSystemDesc.levelRowCol(level)

  val tileLayout = TileLayout(lrc.cols.toInt, lrc.rows.toInt, 512, 512)
  val extent = Extent(-180, -90, 180, 90)
  val tileExtents = TileExtents(extent, tileLayout)

//  var cache: MultibandGeoTiff = prepareCache(thisExtent.cmin, thisExtent.rmin)
//
//  def readFromCache(col: Int, row: Int): MultibandGeoTiff = {
//    prepareCache(col, row)
//    readFromCache(col, row)
//  }

  override def crop(output: TileWriter, col: Int, row: Int, targetWidth: Int, targetHeight: Int): Unit = {
    val tiff: MultibandGeoTiff = tiffReader

    val pe = ProjectedExtent(tileExtents(col, row), LatLng)
    val newTile = tiff.crop(pe.reproject(tiffReader.crs)).tile.resample(targetWidth, targetHeight)

    val newTiff = MultibandGeoTiff(newTile, pe.extent, LatLng)
    val byteArr = GeoTiffWriter.write(newTiff)

    assert(newTiff.tile.bandCount >= 3)

    output.write(catalog, TileCoord(level, row, col), byteArr)
  }
}
