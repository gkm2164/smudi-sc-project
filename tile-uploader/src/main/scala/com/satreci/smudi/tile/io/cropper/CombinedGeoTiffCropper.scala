package com.satreci.smudi.tile.io.cropper
import java.io.File

import com.satreci.smudi.tile.entity.TileExtent
import com.satreci.smudi.tile.io.writer.TileWriter
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.util.{FileRangeReader, StreamingByteReader}

import scala.collection.GenSeq
import scala.util.matching.Regex

case class CombinedGeoTiffCropper(path: String, catalog: String, catalogWriter: TileExtent => Unit) extends ImageCropper {
  implicit def extentTEAlgebra(te: TileExtent) = new {
    def combine(a: TileExtent, b: TileExtent): TileExtent = {
      if (a.level != b.level) {
        if (a.level > b.level) {
          combine(a.prevLevelExtents, b)
        } else /* (te.level < b.level) */ {
          combine(a, b.prevLevelExtents)
        }
      } else {

        val rmin = Math.min(a.rmin, b.rmin)
        val rmax = Math.max(a.rmax, b.rmax)
        val cmin = Math.min(a.cmin, b.cmin)
        val cmax = Math.max(a.cmax, b.cmax)

        TileExtent(a.level, rmin, cmin, rmax, cmax)
      }
    }
    def +(other: TileExtent) = {

      combine(te, other)
    }
  }

  val (rootPath, wildCard) = path.splitAt(path.lastIndexOf("/"))

  val wildCardRegex: Regex = wildCard.substring(1).r

  val files: Array[File] = new File(rootPath).listFiles.filter(file => wildCardRegex.findAllMatchIn(file.getName).nonEmpty)
  val tiffs: Array[MultibandGeoTiff] = files.map(f => GeoTiffReader.readMultiband(StreamingByteReader(FileRangeReader(f)), decompress = false, streaming = true))
  val extents: Array[TileExtent] = tiffs.map(TileExtent(_))
  val totalExtents: TileExtent = extents.reduce(_ + _)

  type GSIm = GenSeq[Int] with Immutable

  override def rowRange: GenSeq[Int] = totalExtents.rmin until totalExtents.rmax /*{
    val rrange: Array[GenSeq[Int]] = extents.map(t => t.rmin until t.rmax)
    rrange.tail.foldLeft(rrange.head)((acc: GenSeq[Int], elem: GenSeq[Int]) => acc ++ elem).par
  }*/

  override def colRange: GenSeq[Int] = totalExtents.cmin until totalExtents.cmax /*{
    val crange: Array[GenSeq[Int]] = extents.map(t => t.cmin until t.cmax)
    crange.tail.foldLeft(crange.head)((acc: GenSeq[Int], elem: GenSeq[Int]) => acc ++ elem).par
  }*/

  val gtc: Array[GeoTiffCropper] = tiffs.map(tf => GeoTiffCropper(tf, catalog, (_: TileExtent) => {}))

  catalogWriter(totalExtents)

  override def crop(output: TileWriter, col: Int, row: Int, targetWidth: Int, targetHeight: Int): Unit =
    gtc.find (c => c.rowRange.exists(_ == row) && c.colRange.exists(_ == col)) match {
      case Some(cropper) => cropper.crop(output, col, row, 512, 512)
      case None => throw new IndexOutOfBoundsException("There's no such a tile") // Doing nothing for now...
    }
}
