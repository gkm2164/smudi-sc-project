package com.satreci.smudi.tile.io

import java.io.{File, RandomAccessFile}

import geotrellis.util.RangeReader

class ImageRangeReader(file: File) extends RangeReader {
  override def totalLength: Long = file.length

  override protected def readClippedRange(start: Long, length: Int): Array[Byte] = {
    val rac = new RandomAccessFile(file, "r")
    val startPosition = start & 0xFFFFFFFFL

    rac.seek(startPosition)

    val readLength = if (start + length > totalLength) totalLength - startPosition else length
    val bytes = Array.ofDim[Byte](readLength.toInt)
    rac.read(bytes)

    rac.close()
    bytes
  }
}

object ImageRangeReader  {
  def apply(path: String): ImageRangeReader  = new ImageRangeReader(new File(path))
}