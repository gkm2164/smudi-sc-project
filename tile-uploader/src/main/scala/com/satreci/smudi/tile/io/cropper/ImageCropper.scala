package com.satreci.smudi.tile.io.cropper

import com.satreci.smudi.tile.io.writer.TileWriter

import scala.collection.GenSeq

trait ImageCropper {
  def rowRange: GenSeq[Int]
  def colRange: GenSeq[Int]
  def crop(output: TileWriter, col: Int, row: Int, targetWidth: Int, targetHeight: Int): Unit
}
