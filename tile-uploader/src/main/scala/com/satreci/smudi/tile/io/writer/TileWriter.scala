package com.satreci.smudi.tile.io.writer

import java.io.OutputStream

import com.satreci.smudi.tile.entity.TileCoord

trait TileWriter {
  def skipWrite(catalog: String, tileCoord: TileCoord): Boolean
  def write(catalog: String, tileCoord: TileCoord, bytes: Array[Byte]): Unit
  def write(catalog: String, tileCoord: TileCoord, writerDelegate: OutputStream => Unit): Unit
}
