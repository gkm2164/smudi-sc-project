package com.satreci.smudi.tile.io.writer

import java.io.OutputStream
import java.net.URI

import com.satreci.smudi.hdfs.HdfsImageFS
import com.satreci.smudi.tile.entity.TileCoord
import com.typesafe.config.Config
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.hdfs.DistributedFileSystem

class HadoopTileWriter(implicit config: Config) extends TileWriter {
  val hdfsRoot = config.getString("smudi.hadoop.address")

  val conf = new Configuration()

  val hdfs = FileSystem.get(new URI(hdfsRoot), conf).asInstanceOf[DistributedFileSystem]

  if (!hdfs.exists(new Path(HdfsImageFS.origPath))) {
    hdfs.mkdirs(new Path(HdfsImageFS.origPath))
  }

  override def skipWrite(catalog: String, tileCoord: TileCoord) = tileCoord match {
    case TileCoord(level, row, col) =>
      val fileName = HdfsImageFS.tilePath(catalog.replace("_", ""), level, row, col)
      hdfs.exists(new Path(fileName))  }

  override def write(catalog: String, tileCoord: TileCoord, bytes: Array[Byte]): Unit = {
    val (level, row, col) = tileCoord match {
      case TileCoord(l, r, c) => (l, r, c)
    }

    val fileName = HdfsImageFS.tilePath(catalog.replace("_", ""), level, row, col)

    if (!hdfs.exists(new Path(fileName))) {
      val outputStream = hdfs.create(new Path(fileName))

      outputStream.write(bytes)
      outputStream.close()
    }
  }

  override def write(catalog: String, tileCoord: TileCoord, writerDelegate: OutputStream => Unit): Unit = {
    val (level, row, col) = tileCoord match {
      case TileCoord(l, r, c) => (l, r, c)
    }

    val fileName = HdfsImageFS.tilePath(catalog.replace("_", ""), level, row, col)
    if (!hdfs.exists(new Path(fileName))) {

      val outputStream = hdfs.create(new Path(fileName))

      /* Unsafe */
      writerDelegate(outputStream)

      outputStream.close()
    }
  }
}
