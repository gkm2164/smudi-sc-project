package com.satreci.smudi.tile.io.cropper

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import com.satreci.smudi.CoordSystemDesc
import com.satreci.smudi.tile.entity.{TileCoord, TileExtent, TileResolution}
import com.satreci.smudi.tile.io.writer.TileWriter
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.writer.GeoTiffWriter
import geotrellis.raster.{ByteArrayTile, MultibandTile}

case class JP2Cropper(folderName: String, catalogName: String, catalogWriter: TileExtent => Unit) extends ImageCropper {
  val level = new File(folderName).listFiles.filter(x => "[0-9]+".r.findAllIn(x.getName).nonEmpty).map(_.getName.toInt).max + 1

  val listColRows = new File(s"$folderName/${level - 1}").listFiles.map(_.getName.toString)
  val pattern = "([0-9]+)_([0-9]+)".r
  val lcrs_ = listColRows.map {
    case pattern(y, z) => (y.toInt, z.toInt)
    case _ => (-1, -1)
  }

  val lcrs =
    (lcrs_.minBy {
      case (x, _) => x
    }._1, lcrs_.minBy {
      case (_, y) => y
    }._2, lcrs_.maxBy {
      case (x, _) => x
    }._1, lcrs_.maxBy {
      case (_, y) => y
    }._2)

  val minPath = s"${lcrs._1}_${lcrs._2}"
  val maxPath = s"${lcrs._3}_${lcrs._4}"

  val regex = s"${catalogName}_${level - 1}_([0-9]+)_([0-9]+).jp2".r

  println(minPath, maxPath)

  val (minCol, maxCol, minRow, maxRow) = {
    val fileList = new File(s"$folderName/${level - 1}/$minPath").listFiles.map(_.getName.toString).map {
      case regex(c, r) => (c.toInt, r.toInt)
    }

    val fileList2 = new File(s"$folderName/${level - 1}/$maxPath").listFiles.map(_.getName.toString).map {
      case regex(c, r) => (c.toInt, r.toInt)
    }

    val fileLists = fileList ++ fileList2

    (fileLists.minBy {
      case (x, _) => x
    }._1, fileLists.maxBy {
      case (x, _) => x
    }._1, fileLists.minBy {
      case (_, y) => y
    }._2, fileLists.maxBy {
      case (_, y) => y
    }._2)
  }

  val (_, maxLevRow, maxLevCol) = CoordSystemDesc.levelRowCol(level) match {
    case TileResolution(l, c, r) => (l.toInt, r.toInt, c.toInt)
  }

  override def rowRange = (0 until maxLevRow).par
  override def colRange = (0 until maxLevCol).par

  catalogWriter(TileExtent(level, 0, 0, maxLevRow, maxLevCol))

  override def crop(output: TileWriter, col: Int, row: Int, targetWidth: Int, targetHeight: Int): Unit = {
    val midPath = s"${col - col % 100}_${row - row % 100}"

    val filePath = s"$folderName/${level - 1}/$midPath/${catalogName}_${level - 1}_${col}_${row}.jp2"

    val bi: BufferedImage = ImageIO.read(new File(filePath))

    val rTile: ByteArrayTile = ByteArrayTile.ofDim(bi.getWidth, bi.getHeight)
    val gTile: ByteArrayTile = ByteArrayTile.ofDim(bi.getWidth, bi.getHeight)
    val bTile: ByteArrayTile = ByteArrayTile.ofDim(bi.getWidth, bi.getHeight)

    for {
      c <- (0 until bi.getWidth).par
      r <- (0 until bi.getHeight).par
    } yield {
      val pixel = bi.getRGB(c, r)

      val red = (pixel >> 16) & 0xFF
      val green = (pixel >> 8) & 0xFF
      val blue = pixel & 0xFF
      rTile.set(c, r, red)
      gTile.set(c, r, green)
      bTile.set(c, r, blue)
    }

    val mbt: MultibandTile = MultibandTile.apply(rTile, gTile, bTile)
    val pe = TileCoord(level, row, col).getTileExtent
    val tiff: MultibandGeoTiff = MultibandGeoTiff(mbt, pe.extent, pe.crs).mapTile(_.resample(targetWidth, targetHeight))

    output.write(catalogName, TileCoord(level, row, col), GeoTiffWriter.write(tiff))

    println(s"$filePath written.")
  }
}
