package com.satreci.smudi.tile.cache
case class TiffNotFoundException(str: String) extends Exception(str)

