package com.satreci.smudi.tile.cache

import java.awt.image.BufferedImage
import java.io.{ByteArrayInputStream, ByteArrayOutputStream, InputStream}
import java.net.URI
import javax.imageio.ImageIO

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.satreci.smudi.hdfs.HdfsImageFS
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.messages._
import com.satreci.smudi.tile.entity.TileCoord
import com.typesafe.config.Config
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.spark.io.hadoop.HdfsRangeReader
import geotrellis.util.StreamingByteReader
import org.apache.commons.compress.utils.IOUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

class TileCache(catalog: String, tileCacheInventory: ActorRef, cluster: ActorRef,
                writer: (String, BufferedImage) => Unit)(implicit imageInventory: ImageInventory, config: Config)
    extends Actor with ActorLogging {

  var cacheTable = CacheTable(Map())

  override def receive: PartialFunction[Any, Unit] = {
    case CacheClearAll =>
      tileCacheInventory ! InvalidateAll
      cacheTable = CacheTable(Map())

    case InvalidateCacheEntry(l, r, c) =>
      tileCacheInventory ! SetCacheState(l, r, c, CacheState.Invalid)
      cacheTable = cacheTable.invalidateCacheEntry(TileCoord(l, r, c))

    case LoadCacheEntryFire(l, r, c) =>
      tileCacheInventory ! SetCacheState(l, r, c, CacheState.Preparing)
      try {
        val (ct, _) = cacheTable.loadCacheEntry(TileCoord(l, r, c))
        cacheTable = ct
        tileCacheInventory ! SetCacheState(l, r, c, CacheState.Valid)
      } catch {
        case e: Exception =>
          println(e)
          tileCacheInventory ! SetCacheState(l, r, c, CacheState.Invalid)
      }

    case LoadCacheEntry(l, r, c) =>
      tileCacheInventory ! SetCacheState(l, r, c, CacheState.Preparing)

      try {
        val (ct, entry) = cacheTable.loadCacheEntry(TileCoord(l, r, c))
        cacheTable = ct
        println(s"${TileCoord(l, r, c)} loaded successfully")
        tileCacheInventory ! SetCacheState(l, r, c, CacheState.Valid)
        sender ! entry
      } catch {
        case e: Exception =>
          println(e)
          tileCacheInventory ! SetCacheState(l, r, c, CacheState.Invalid)
          sender ! None
      }
  }

  case class CacheTable(cacheEntry: Map[TileCoord, InputStream]) {
    def invalidateCacheEntry(coord: TileCoord): CacheTable = {
      CacheTable(if (cacheEntry contains coord)
        cacheEntry - coord
      else
        cacheEntry)
    }

    def validateCacheEntry(coord: TileCoord, is: InputStream): CacheTable = {
      CacheTable((if (cacheEntry contains coord)
        cacheEntry - coord
      else
        cacheEntry) + (coord -> is))
    }

    val hdfsHost: String = config.getString("smudi.hadoop.address")
    val conf: Configuration = new Configuration()
    val hdfs: FileSystem = FileSystem.get(new URI(hdfsHost), conf)

    def loadCacheEntry(coord: TileCoord): (CacheTable, Array[Byte]) = {
      val (level, row, col) = coord match {
        case TileCoord(l, r, c) => (l, r, c)
      }
      val inputFilePath = HdfsImageFS.tilePath(catalog, level, row, col)
      val outputCachePath = HdfsImageFS.tileCachePath(catalog, level, row, col)

      if (!hdfs.exists(new Path(outputCachePath))) {
        if (!hdfs.exists(new Path(inputFilePath))) {
          throw TiffNotFoundException(s"Tiff tile ($inputFilePath) should be prepared first.")
        }

        val (outputDirPath, _) = outputCachePath.splitAt(outputCachePath.lastIndexOf("/"))

        if (!hdfs.exists(new Path(outputDirPath))) {
          hdfs.mkdirs(new Path(outputDirPath))
        }

        val hrr = HdfsRangeReader(new Path(s"$hdfsHost$inputFilePath"), conf)
        val tiff = GeoTiffReader.readMultiband(StreamingByteReader(hrr), decompress = false, streaming = true)

        println(s"Bands count for $inputFilePath: ${tiff.tile.bandCount}")

        val pngBytes = tiff.mapTile(_.resample(512, 512)).tile.renderPng.bytes
        val bais = new ByteArrayInputStream(pngBytes)
        val bi = ImageIO.read(bais)

        val tempFileName: String = s"dds_for_${System.currentTimeMillis}.dds"
        println("Exported to png. Writing dds...")
        writer(tempFileName, bi)
        println(s"$inputFilePath has been cached to $outputCachePath")

        hdfs.copyFromLocalFile(true, new Path(tempFileName), new Path(outputCachePath))
      }

      val is = hdfs.open(new Path(outputCachePath)).getWrappedStream
      val baos = new ByteArrayOutputStream()
      IOUtils.copy(is, baos)
      val ba = baos.toByteArray
      is.close()
      baos.close()
      (validateCacheEntry(coord, is), ba)
    }
  }
}
