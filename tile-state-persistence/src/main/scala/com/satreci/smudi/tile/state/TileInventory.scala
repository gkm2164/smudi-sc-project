package com.satreci.smudi.tile.state

import akka.actor._
import akka.cluster.client.ClusterClient
import akka.persistence._
import com.satreci.smudi.actors.ActorAddress._
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.messages._
import com.satreci.smudi.tile.entity.TileCoord

import scala.collection.mutable
import scala.concurrent.ExecutionContextExecutor

class TileInventory(catalog: String, cluster: ActorRef)(implicit imageInventory: ImageInventory)
  extends PersistentActor with ActorLogging {

  import TileStatus._

  implicit val dispatcher: ExecutionContextExecutor = context.dispatcher

  @volatile var state = new Tiles(mutable.Map())
  var waitQueue: Map[TileCoord, mutable.Queue[TileState => Unit]] = Map()

  def persistenceId = s"TileStatus_$catalog"

  def updateState: TileOperation => Unit = {
    case ClearAll => state = new Tiles(mutable.Map())
    case Clear(level, row, col) => state.reset(TileCoord(level, row, col))
    case SetTileState(level, row, col, tileState) => state.setTileState(TileCoord(level, row, col), tileState)
    case _ =>
  }

  override def receiveCommand: Receive = {
    case ClearAll => persist(ClearAll)(updateState)
    case Clear(l, r, c) => persist(Clear(l, r, c))(updateState)
    case SetTileState(l, r, c, s) => {
      persist(SetTileState(l, r, c, s))(updateState)

      val key = TileCoord(l, r, c)
      s match {
        case TileState.Available | TileState.Unavailable =>
          log.info(s"[TileStatus] Tile $key state has changed to $s)")
          waitQueue.synchronized {
            if (waitQueue contains key) {
              log.info(s"[TileStatus] Wait queue for $key have ${waitQueue(key).length} tasks. Now started")

              while (waitQueue(key).nonEmpty) {
                log.info("[TileStatus] Dequeueing")
                waitQueue(key).dequeue()(s)
              }

              waitQueue -= key
            }
          }
        case _ =>
          log.warning(s"[TileStatus] Tile $key is not ready")
      }
    }

    case WaitTileResult(l, r, c, cb) => {
      val key = TileCoord(l, r, c)
      waitQueue.synchronized {
        log.info("Adding to queue")
        if (!(waitQueue contains key)) {
          waitQueue += (key -> mutable.Queue())
        }
        waitQueue(key).enqueue(cb)
      }

      self ! RefreshTileState(l, r, c)
    }

    case RefreshTileState(l, r, c) =>
      val tileState = state.getTileState(TileCoord(l, r, c))
      self ! SetTileState(l, r, c, tileState)

    case WaitAvailabilityCluster(cc, l, r, c) => {
      self ! WaitTileResult(l, r, c, (tileState) => {
        cluster ! ClusterClient.Send(TileSpark(cc),
          TileStateResult(l, r, c, tileState), localAffinity = false)
      })
    }

    case DispatchAll => {
      waitQueue.synchronized {
        for {
          tileCoord <- waitQueue.keys
        } yield {
          val tileState = state.getTileState(tileCoord)
          if (tileState == TileState.Available || tileState == TileState.Unavailable)
            while (waitQueue.nonEmpty)
              waitQueue(tileCoord).dequeue()(tileState)
        }
      }
    }

    case GetTileState(l, r, c) => sender ! state.getTileState(TileCoord(l, r, c))
    case ShowWaitQueue => sender ! waitQueue.toList.map {
      case (TileCoord(l, r, c), queues) => (TileStateResult(l, r, c, state.getTileState(TileCoord(l, r, c))), queues)
    }.toMap.toString

    case _ => println("???")
  }

  override def receiveRecover: PartialFunction[Any, Unit] = {
//    case op: TileOperation =>
//      log.info(s"[TileStatus] Recover from journal with $op")
//      updateState(op)
//    case RecoveryCompleted =>
//      log.info("[TileStatus] Tile Status recover completed")
    case _ => log.info("[TileStatus] Ignore recovery")
  }

  def loadMaxLevelMap(): Unit = {
    val level = imageInventory.images(catalog).level.max
    for {
      row <- imageInventory.images(catalog)(level).row.min until imageInventory.images(catalog)(level).row.max
      col <- imageInventory.images(catalog)(level).col.min until imageInventory.images(catalog)(level).col.max
    } yield {
      if (imageInventory.isValidQuery(RequestQuery(catalog, level, row, col)))
        self ! SetTileState(level, row, col, TileState.Available)
    }
  }

  class Tiles(var map: mutable.Map[TileCoord, TileState]) {

    def reset(tc: TileCoord): Unit = {
      map.synchronized {
        if (map contains tc) {
          map -= tc
        }
      }
    }

    def setTileState(tc: TileCoord, tileState: TileState): Unit = {
      map.synchronized {
        if (map contains tc)
          map -= tc
        map += (tc -> tileState)
      }
    }

    def getTileState(tc: TileCoord): TileState = {
      log.info("[TileStatus] Came here for get preparing state")
      if (map contains tc)
        map(tc)
      else
        TileState.Unknown
    }
  }

  // loadMaxLevelMap()
}
