package com.satreci.smudi.tile.cache

import akka.actor.{ActorLogging, ActorRef}
import akka.persistence.PersistentActor
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.messages._
import com.satreci.smudi.tile.entity.TileCoord

class TileCacheInventory(catalog: String, cluster: ActorRef)(implicit imageInventory: ImageInventory)
  extends PersistentActor with ActorLogging {
  val name = s"TileCacheFor$catalog"

  import scala.collection.concurrent.{Map, TrieMap}

  var state: TileCache = TileCache(TrieMap.empty[TileCoord, CacheState])

  def updateCache: TileCacheInventoryOperation => Unit = {
    case InvalidateAll =>
      state.invalidateAll()
    case InvalidateCache(level, row, col) =>
      state.invalidateCacheState(TileCoord(level, row, col))
    case SetCacheState(level, row, col, cacheState) =>
      state.setCacheState(TileCoord(level, row, col), cacheState)
    case _ => log.info("Undefined operations")
  }

  override def receiveRecover: PartialFunction[Any, Unit] = {
    case op: TileCacheInventoryOperation => updateCache(op)
    case _ =>
  }

  override def receiveCommand: PartialFunction[Any, Unit] = {
    case InvalidateAll => persist(InvalidateAll)(updateCache)
    case InvalidateCache(level, row, col) => persist(InvalidateCache(level, row, col))(updateCache)
    case SetCacheState(level, row, col, cacheState) => persist(SetCacheState(level, row, col, cacheState))(updateCache)
    case GetCacheState(level, row, col) => sender ! state.getCacheState(TileCoord(level, row, col))
  }

  override def persistenceId: String = name


  case class TileCache(cacheState: Map[TileCoord, CacheState]) {
    def invalidateAll(): Unit = {
      cacheState.clear()
    }

    def invalidateCacheState(coord: TileCoord): Unit = {
      cacheState.synchronized {
        if (cacheState contains coord)
          cacheState -= coord
      }
    }

    def setCacheState(tc: TileCoord, state: CacheState): Option[CacheState] =
      cacheState.put(tc, state)

    def getCacheState(tc: TileCoord): CacheState =
      cacheState.getOrElse(tc, CacheState.Invalid)
  }

}
