package com.satreci.smudi.tile.cache

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, File}
import java.net.URI
import javax.imageio.ImageIO

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.cluster.client.ClusterClient
import akka.pattern._
import akka.util.Timeout
import com.satreci.smudi.Bootstrap.system
import com.satreci.smudi.actors.ActorAddress._
import dds.ddsutil.DDSUtil
import com.satreci.smudi.hdfs.HdfsImageFS
import com.satreci.smudi.messages.{CacheState, GetCacheState, SetCacheState}
import com.satreci.smudi.tile.messages.{CacheCreate, CacheRead}
import com.typesafe.config.Config
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.spark.io.hadoop.HdfsRangeReader
import geotrellis.util.StreamingByteReader
import org.apache.commons.compress.utils.IOUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.Success



class TileCacheIO(catalog: String, cluster: ActorRef)(implicit config: Config) extends Actor with ActorLogging {

  def tileCreate(level: Int, row: Int, col: Int) = {
    val inputFilePath = HdfsImageFS.tilePath(catalog, level, row, col)
    val outputCachePath = HdfsImageFS.tileCachePath(catalog, level, row, col)

    val conf = new Configuration()
    val hdfsHost: String = config.getString("smudi.hadoop.address")
    val hdfs = FileSystem.get(new URI(hdfsHost), conf)

    if (!hdfs.exists(new Path(outputCachePath))) {
      if (!hdfs.exists(new Path(inputFilePath))) {
        throw TiffNotFoundException(s"Tiff tile ($inputFilePath) should be prepared first.")
      }

      val (outputDirPath, _) = outputCachePath.splitAt(outputCachePath.lastIndexOf("/"))

      if (!hdfs.exists(new Path(outputDirPath))) {
        hdfs.mkdirs(new Path(outputDirPath))
      }

      val hrr = HdfsRangeReader(new Path(s"$hdfsHost$inputFilePath"), conf)
      val tiff = GeoTiffReader.readMultiband(StreamingByteReader(hrr), decompress = false, streaming = true)

      println(s"Bands count for $inputFilePath: ${tiff.tile.bandCount}")

      val pngBytes = tiff.mapTile(_.resample(512, 512)).tile.renderPng.bytes
      val bais = new ByteArrayInputStream(pngBytes)
      val bi = ImageIO.read(bais)

      val tempFileName: String = s"dds_for_${System.currentTimeMillis}.dds"
      println("Exported to png. Writing dds...")
      DDSUtil.write(new File(tempFileName), bi)
      println(s"$inputFilePath has been cached to $outputCachePath")

      hdfs.copyFromLocalFile(true, new Path(tempFileName), new Path(outputCachePath))
    }

    val is = hdfs.open(new Path(outputCachePath)).getWrappedStream
    val baos = new ByteArrayOutputStream()
    IOUtils.copy(is, baos)
    val ba = baos.toByteArray
    is.close()
    baos.close()
    hdfs.close()

    cluster ! ClusterClient.Send(TileCache(catalog), SetCacheState(level, row, col, CacheState.Valid), localAffinity = false)
  }

  def readFile(path: String) = {
    val conf = new Configuration()
    val hdfsHost = config.getString("smudi.hadoop.address")
    val hdfs = FileSystem.get(new URI(hdfsHost), conf)

    val file = hdfs.open(new Path(path))
    file.getPos

  }

  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  def tileRead(level: Int, row: Int, col: Int): Unit = {
    implicit val timeout: Timeout = Timeout(10 seconds)
    val cacheState = cluster ? ClusterClient.Send(TileCache(catalog), GetCacheState(level, row, col), localAffinity = false)

    cacheState onComplete {
      case Success(CacheState.Valid) => sender ! Some(readFile(HdfsImageFS.tileCachePath(catalog, level, row, col)))
      case _ => sender ! None
    }
  }

  override def receive: Receive = {
    case CacheCreate(l, r, c) => tileCreate(l, r, c)
    case CacheRead(l, r, c) => tileRead(l, r, c)
  }
}
