package com.satreci.smudi.tile.messages

trait TileCacheMessage
case class CacheCreate(level: Int, row: Int, col: Int) extends TileCacheMessage
case class CacheRead(level: Int, row: Int, col: Int) extends TileCacheMessage