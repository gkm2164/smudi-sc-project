package com.satreci.smudi

import java.awt.image.BufferedImage
import java.io.File
import java.util.concurrent.ConcurrentHashMap

import akka.actor.{Actor, ActorPath, ActorRef, ActorSystem, Address, Props}
import akka.cluster.Cluster
import akka.cluster.client.{ClusterClient, ClusterClientReceptionist, ClusterClientSettings}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import dds.ddsutil.DDSUtil
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.mailbox.MonitorActor
import com.satreci.smudi.messages.LoadCatalog
import com.satreci.smudi.tile.cache.{TileCache, TileCacheInventory}
import com.satreci.smudi.tile.state.TileInventory
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.io.StdIn

object Bootstrap extends App {
  implicit val config: Config =
    if (args.length > 0) {
      ConfigFactory.parseString(s"akka.remote.netty.tcp.port=${args(0)}")
        .withFallback(ConfigFactory.load())
    } else {
      ConfigFactory.load()
    }

  implicit val system: ActorSystem = ActorSystem("smudi-clusters", config = config)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val requestTimeout: Timeout = akka.util.Timeout(1000 seconds)

  implicit val imageInventory: ImageInventory = new ImageInventory

  val seedAddr = Address(
    config.getString("smudi.cluster-seed.protocol"),
    config.getString("smudi.cluster-seed.name"),
    config.getString("smudi.cluster-seed.host"),
    config.getInt("smudi.cluster-seed.port")
  )

  println(seedAddr.toString)

  val initialContacts = Set(
    ActorPath.fromString(s"${seedAddr.toString}/system/receptionist")
  )

  val c = system.actorOf(
    ClusterClient.props(
      ClusterClientSettings(system).withInitialContacts(initialContacts)
    ), "client")

  private def makeTileInventory(catalog: String): (String, ActorRef) =
    catalog -> system.actorOf(Props(new TileInventory(catalog, c) with MonitorActor), s"${catalog}TileInventory")

  private def makeTileCacheInventory(catalog: String): (String, ActorRef) =
    catalog -> system.actorOf(Props(new TileCacheInventory(catalog, c) with MonitorActor), s"${catalog}TileCacheInventory")

  private def makeTileCache(catalog: String): (String, ActorRef) =
    catalog -> system.actorOf(Props(new TileCache(catalog, tileCacheInventory.get(catalog), c, ddsWriter) with MonitorActor), s"${catalog}TileCache")

  val catalogs = imageInventory.getCatalogs

  val tileInventory: ConcurrentHashMap[String, ActorRef] =
    catalogs.map(makeTileInventory).toMap
      .foldLeft(new ConcurrentHashMap[String, ActorRef]) { (z, tuple) =>
        tuple match {
          case (name, actor) =>
            z.put(name, actor)
            z
        }
      }
  val tileCacheInventory: ConcurrentHashMap[String, ActorRef] = catalogs.map(makeTileCacheInventory).toMap
    .foldLeft(new ConcurrentHashMap[String, ActorRef]) { (z, tuple) =>
      tuple match {
        case (name, actor) =>
          z.put(name, actor)
          z
      }
    }
  val tileCache: ConcurrentHashMap[String, ActorRef] = catalogs.map(makeTileCache).toMap
    .foldLeft(new ConcurrentHashMap[String, ActorRef]) { (z, tuple) =>
      tuple match {
        case (name, actor) =>
          z.put(name, actor)
          z
      }
    }

  val cluster = Cluster(system)

  cluster.join(seedAddr)

  def ddsWriter(tempFileName: String, bi: BufferedImage): Unit = synchronized {
    DDSUtil.write(new File(tempFileName), bi)
  }

  import collection.JavaConverters._

  val lists: List[ActorRef] =
    tileInventory.values.asScala.toList :::
      tileCacheInventory.values.asScala.toList :::
      tileCache.values.asScala.toList

  lists.foreach(actors => {
    println("Path " + actors.path.toStringWithoutAddress + " is added")
    ClusterClientReceptionist(system).registerService(actors)
  })

  ClusterClientReceptionist(system).registerService(system.actorOf(
    Props(new Actor() {

      def registerCatalog(catalog: String): Unit = {
        val (inventoryName, inventoryActor) = makeTileInventory(catalog)
        val (cacheInventoryName, cacheInventoryActor) = makeTileCacheInventory(catalog)
        val (cacheName, cacheActor) = makeTileCache(catalog)

        tileInventory.put(inventoryName, inventoryActor)
        tileCacheInventory.put(cacheInventoryName, cacheInventoryActor)
        tileCache.put(cacheName, cacheActor)
      }

      override def receive: Receive = {
        case LoadCatalog(catalog) => registerCatalog(catalog)
        case _ => println("Invalid operations")
      }
    }), "CacheLoader"))

  StdIn.readLine()

  println("Terminating System...")

  cluster.leave(seedAddr)

  system.terminate()
}