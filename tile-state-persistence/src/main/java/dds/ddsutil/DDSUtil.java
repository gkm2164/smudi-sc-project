package dds.ddsutil;

import dds.compression.DXTBufferCompressor;
import dds.jogl.DDSImage;
import jsquish.Squish;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Easy loading, saving and manipulation of DDSImages and DXT-Compression.
 * 
 * @author danielsenff
 *
 */
public class DDSUtil {
    private static BufferedImage convert(final BufferedImage srcImage, final int destImgType) {
        BufferedImage img = new BufferedImage(srcImage.getWidth(), srcImage.getHeight(), destImgType);
        Graphics2D g2d = img.createGraphics();
        g2d.drawImage(srcImage, 0, 0, null);
        g2d.dispose();
        return img;
    }

    private static ByteBuffer[] getDXTCompressedBuffer(BufferedImage bi) {
        ByteBuffer[] buffer = new ByteBuffer[1];
        DXTBufferCompressor compImg = new DXTBufferCompressor(bi, Squish.CompressionType.DXT1);
        buffer[0] = compImg.getByteBuffer();
        return buffer;
    }

    /**
     * Writes a DDS-Image file to disk.
     *
     * @param dstFile
     * @param sourceImage
     * @throws IOException
     */
    public static void write(final File dstFile,
                             BufferedImage sourceImage) throws IOException {
        //convert RGB to RGBA image
        if (!sourceImage.getColorModel().hasAlpha())
            sourceImage = convert(sourceImage, BufferedImage.TYPE_4BYTE_ABGR);

        int width = sourceImage.getWidth();
        int height = sourceImage.getHeight();

        ByteBuffer[] mipmapBuffer = getDXTCompressedBuffer(sourceImage);
        DDSImage image = DDSImage.createFromData(width, height, mipmapBuffer);

        image.write(dstFile);
    }
}