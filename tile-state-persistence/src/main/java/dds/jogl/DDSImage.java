/*
 * Copyright (c) 2005 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * - Redistribution of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistribution in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * Neither the name of Sun Microsystems, Inc. or the names of
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN
 * MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL NOT BE LIABLE FOR
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR
 * ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE
 * DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY,
 * ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF
 * SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You acknowledge that this software is not designed or intended for use
 * in the design, construction, operation or maintenance of any nuclear
 * facility.
 *
 * Sun gratefully acknowledges that this software was originally authored
 * and developed by Kenneth Bradley Russell and Christopher John Kline.
 */

package dds.jogl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

/** A reader and writer for DirectDraw Surface (.dds) files, which are
    used to describe textures. These files can contain multiple mipmap
    levels in one file. This class is currently minimal and does not
    support all of the possible file formats. */

public class DDSImage {

    /**
     * Simple class describing images and data; does not encapsulate
     * image format information. User is responsible for transmitting
     * that information in another way.
     */

    private ByteBuffer buf;
    private Header header;

    //
    // Selected bits in header flags
    //
    private static final int DDSD_CAPS = 0x00000001; // Capacities are valid
    private static final int DDSD_HEIGHT = 0x00000002; // Height is valid
    private static final int DDSD_WIDTH = 0x00000004; // Width is valid
    private static final int DDSD_PIXELFORMAT = 0x00001000; // ddpfPixelFormat is valid
    private static final int DDSD_MIPMAPCOUNT = 0x00020000; // Mip map count is valid
    private static final int DDSD_LINEARSIZE = 0x00080000; // dwLinearSize is valid

    private static final int DDPF_FOURCC = 0x00000004; // FourCC code is valid

    // The following are also valid FourCC codes
    private static final int D3DFMT_DXT1 = 0x31545844;

    private DDSImage(int width, int height, ByteBuffer[] mipmapData) throws IllegalArgumentException {
        initFromData(width, height, mipmapData);
    }

    /**
     * Creates a new DDSImage from data supplied by the user. The
     * resulting DDSImage can be written to disk using the write()
     * method.
     *
     * @param width      the width in pixels of the topmost mipmap image
     * @param height     the height in pixels of the topmost mipmap image
     * @param mipmapData the data for each mipmap level of the resulting
     *                   DDSImage; either only one mipmap level should
     *                   be specified, or they all must be
     * @return DDS image object
     * @throws IllegalArgumentException if the data does not match the
     *                                  specified arguments
     */
    public static DDSImage createFromData(int width,
                                          int height,
                                          ByteBuffer[] mipmapData) throws IllegalArgumentException {
        return new DDSImage(width, height, mipmapData);
    }

    public void write(FileOutputStream fos) throws IOException {
        FileChannel chan = fos.getChannel();
        // Create ByteBuffer for header in case the start of our
        // ByteBuffer isn't actually memory-mapped
        ByteBuffer hdr = ByteBuffer.allocate(Header.writtenSize());
        hdr.order(ByteOrder.LITTLE_ENDIAN);
        header.write(hdr);
        hdr.rewind();
        chan.write(hdr);
        buf.position(Header.writtenSize());
        chan.write(buf);
        chan.force(true);
        chan.close();
    }

    /**
     * Writes this DDSImage to the specified file name.
     *
     * @param file File object to write to
     * @throws IOException if an I/O exception occurred
     */
    public void write(File file) throws IOException {
        FileOutputStream stream = new FileOutputStream(file);
        write(stream);
        stream.close();
    }

    private static final int MAGIC = 0x20534444;

    static class Header {
        int size;                 // size of the DDSURFACEDESC structure
        int flags;                // determines what fields are valid
        int height;               // height of surface to be created
        int width;                // width of input surface
        int pitchOrLinearSize;
        int backBufferCountOrDepth;
        int mipMapCountOrAux;     // number of mip-map levels requested (in this context)
        int alphaBitDepth;        // depth of alpha buffer requested
        int reserved1;            // reserved
        int surface;              // pointer to the associated surface memory
        // NOTE: following two entries are from DDCOLORKEY data structure
        // Are overlaid with color for empty cubemap faces (unused in this reader)
        int colorSpaceLowValue;
        int colorSpaceHighValue;
        int destBltColorSpaceLowValue;
        int destBltColorSpaceHighValue;
        int srcOverlayColorSpaceLowValue;
        int srcOverlayColorSpaceHighValue;
        int srcBltColorSpaceLowValue;
        int srcBltColorSpaceHighValue;
        // NOTE: following entries are from DDPIXELFORMAT data structure
        // Are overlaid with flexible vertex format description of vertex
        // buffers (unused in this reader)
        int pfSize;                 // size of DDPIXELFORMAT structure
        int pfFlags;                // pixel format flags
        int pfFourCC;               // (FOURCC code)
        // Following five entries have multiple interpretations, not just
        // RGBA (but that's all we support right now)
        int pfRGBBitCount;          // how many bits per pixel
        int pfRBitMask;             // mask for red bits
        int pfGBitMask;             // mask for green bits
        int pfBBitMask;             // mask for blue bits
        int pfABitMask;             // mask for alpha channel
        int ddsCaps1;               // Texture and mip-map flags
        int ddsCaps2;               // Advanced capabilities including cubemap support
        int ddsCapsReserved1;
        int ddsCapsReserved2;
        int textureStage;           // stage in multitexture cascade

        // buf must be in little-endian byte order
        void write(ByteBuffer buf) {
            buf.putInt(MAGIC);
            buf.putInt(size);
            buf.putInt(flags);
            buf.putInt(height);
            buf.putInt(width);
            buf.putInt(pitchOrLinearSize);
            buf.putInt(backBufferCountOrDepth);
            buf.putInt(mipMapCountOrAux);
            buf.putInt(alphaBitDepth);
            buf.putInt(reserved1);
            buf.putInt(surface);
            buf.putInt(colorSpaceLowValue);
            buf.putInt(colorSpaceHighValue);
            buf.putInt(destBltColorSpaceLowValue);
            buf.putInt(destBltColorSpaceHighValue);
            buf.putInt(srcOverlayColorSpaceLowValue);
            buf.putInt(srcOverlayColorSpaceHighValue);
            buf.putInt(srcBltColorSpaceLowValue);
            buf.putInt(srcBltColorSpaceHighValue);
            buf.putInt(pfSize);
            buf.putInt(pfFlags);
            buf.putInt(pfFourCC);
            buf.putInt(pfRGBBitCount);
            buf.putInt(pfRBitMask);
            buf.putInt(pfGBitMask);
            buf.putInt(pfBBitMask);
            buf.putInt(pfABitMask);
            buf.putInt(ddsCaps1);
            buf.putInt(ddsCaps2);
            buf.putInt(ddsCapsReserved1);
            buf.putInt(ddsCapsReserved2);
            buf.putInt(textureStage);
        }

        private static int size() {
            return 124;
        }

        private static int pfSize() {
            return 32;
        }

        private static int writtenSize() {
            return 128;
        }
    }

    private void initFromData(int width,
                              int height,
                              ByteBuffer[] mipmapData) throws IllegalArgumentException {
        // Check size of mipmap data compared against format, width and
        // height
        int topmostMipmapSize = computeCompressedBlockSize(width, height);

        // Now check the mipmaps against this size
        int curSize = topmostMipmapSize;
        int mipmapWidth = width;
        int mipmapHeight = height;
        int totalSize = 0;
        for (int i = 0; i < mipmapData.length; i++) {
            if (mipmapData[i].remaining() != curSize) {
                throw new IllegalArgumentException("Mipmap level " + i +
                        " didn't match expected data size (expected " + curSize + ", got " +
                        mipmapData[i].remaining() + ")");
            }
            /* Change Daniel Senff
             * I got the problem, that MipMaps below the dimension of 8x8 blocks with DXT5
             * where assume smaller than they are created.
             * Assumed: < 16byte where 16byte where used by the compression. */
            // size calculation for compressed mipmaps
            if (mipmapWidth > 1) mipmapWidth /= 2;
            if (mipmapHeight > 1) mipmapHeight /= 2;
            curSize = computeCompressedBlockSize(mipmapWidth, mipmapHeight);
            totalSize += mipmapData[i].remaining();
        }

        // OK, create one large ByteBuffer to hold all of the mipmap data
        totalSize += Header.writtenSize();
        ByteBuffer buf = ByteBuffer.allocate(totalSize);
        buf.position(Header.writtenSize());
        for (int i = 0; i < mipmapData.length; i++) {
            buf.put(mipmapData[i]);
        }
        this.buf = buf;

        // Allocate and initialize a Header
        header = new Header();
        header.size = Header.size();
        header.flags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
        if (mipmapData.length > 1) {
            header.flags |= DDSD_MIPMAPCOUNT;
            header.mipMapCountOrAux = mipmapData.length;
        }
        header.width = width;
        header.height = height;
        header.flags |= DDSD_LINEARSIZE;
        header.pfFlags |= DDPF_FOURCC;
        header.pfFourCC = D3DFMT_DXT1;

        header.pitchOrLinearSize = topmostMipmapSize;
        header.pfSize = Header.pfSize();
        // Not sure whether we can get away with leaving the rest of the
        // header blank
    }

    private static int computeCompressedBlockSize(int width, int height) {
        return ((width + 3) / 4) * ((height + 3) / 4) * 8;
    }
}