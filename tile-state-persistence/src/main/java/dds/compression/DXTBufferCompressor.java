/**
 * 
 */
package dds.compression;

import com.google.common.base.Stopwatch;
import dds.ddsutil.ByteBufferedImage;
import jsquish.Squish;
import jsquish.Squish.CompressionType;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import java.util.zip.DataFormatException;


/**
 * Compressor for DXT-Compression
 * @author danielsenff
 *
 */
public class DXTBufferCompressor {

    private byte[] byteData;
    private Dimension dimension;
    private Squish.CompressionType compressionType;

    public DXTBufferCompressor(final BufferedImage image,
                               final Squish.CompressionType compressionType) {
        this(ByteBufferedImage.convertBIintoARGBArray(image),
                new Dimension(image.getWidth(null), image.getHeight(null)),
                compressionType);
    }

    private DXTBufferCompressor(final byte[] data,
                                final Dimension dimension,
                                final Squish.CompressionType compressionType) {
        this.byteData = data;
        this.dimension = dimension;
        this.compressionType = compressionType;
    }

    public ByteBuffer getByteBuffer() {
        byte[] compressedData;
        try {

            // the data-Array given to the squishCompressToArray is expected to be
            // width * height * 4 -> with RGBA, which means, if we got RGB, we need to add A!
            if (byteData.length < dimension.height * dimension.width * 4) {
                System.out.println("blow up array from RGB to ARGB");
                byteData = convertRGBArraytiRGBAArray(byteData, dimension);
            }

            compressedData = squishCompressToArray(byteData, dimension.width, dimension.height, compressionType);
            return ByteBuffer.wrap(compressedData);
        } catch (DataFormatException e) {
            e.printStackTrace();
        }
        return null;

    }

    private byte[] convertRGBArraytiRGBAArray(byte[] data, final Dimension dimension) {

        int rgbLength = data.length;
        int rgbaLength = dimension.width * dimension.height * 4;

        byte[] rgbaBuffer = new byte[rgbaLength];

        // populate new array
        // we always copy 3 byte chunks, skip one byte, which we set to 255 and take the next 3 byte
        int loopN = 0;
        for (int i = 0; i < rgbLength; i = i + 3) {
            System.arraycopy(data, i, rgbaBuffer, i + loopN, 3);
            loopN++;
        }


        return rgbaBuffer;
    }

    private static byte[] squishCompressToArray(final byte[] rgba,
                                                final int width,
                                                final int height,
                                                final CompressionType compressionType) throws DataFormatException {

        Stopwatch sw = new Stopwatch();

        // expected array length
        int length = width * height * 4;
        if (rgba.length != length)
            throw new DataFormatException("unexpected length:" +
                    rgba.length + " instead of " + length);

        sw.start();

        int storageRequirements = Squish.getStorageRequirements(width, height, compressionType);
        sw.stop();

        long storageReq = sw.elapsed(TimeUnit.MILLISECONDS);

        sw.reset();
        sw.start();

        Squish squish = new Squish();

        byte[] ret = squish.compressImage(rgba,
                width,
                height,
                new byte[storageRequirements],
                compressionType,
                Squish.CompressionMethod.RANGE_FIT);

        sw.stop();

        long compressReq = sw.elapsed(TimeUnit.MILLISECONDS);

        System.out.println("StorageReq: " + storageReq + "ms, Compress req: " + compressReq);

        return ret;
    }
}