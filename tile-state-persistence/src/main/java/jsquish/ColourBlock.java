/* -----------------------------------------------------------------------------

	Copyright (c) 2006 Simon Brown                          si@sjbrown.co.uk

	Permission is hereby granted, free of charge, to any person obtaining
	a copy of this software and associated documentation files (the
	"Software"), to	deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish,
	distribute, sublicense, and/or sell copies of the Software, and to
	permit persons to whom the Software is furnished to do so, subject to
	the following conditions:

	The above copyright notice and this permission notice shall be included
	in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
	CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

   -------------------------------------------------------------------------- */

package jsquish;

import java.util.Arrays;

import static java.lang.Math.round;
import static jsquish.CompressorColourFit.*;

final class ColourBlock {

	private static final int[] remapped = new int[16];

	private ColourBlock() {
	}

	private static int floatTo565(final Vec colour) {
		// get the components in the correct range
		final int r = round(GRID_X * colour.x());
		final int g = round(GRID_Y * colour.y());
		final int b = round(GRID_Z * colour.z());

		// pack into a single value
		return (r << 11) | (g << 5) | b;
	}

	private static void writeColourBlock(final int a, final int b, final int[] indices, final byte[] block, final int offset) {
		// write the endpoints
		block[offset + 0] = (byte) (a & 0xff);
		block[offset + 1] = (byte) (a >> 8);
		block[offset + 2] = (byte) (b & 0xff);
		block[offset + 3] = (byte) (b >> 8);

		// write the indices
		for (int i = 0; i < 4; ++i) {
			final int index = 4 * i;
			block[offset + 4 + i] = (byte) (indices[index + 0] | (indices[index + 1] << 2) | (indices[index + 2] << 4) | (indices[index + 3] << 6));
		}
	}

	static void writeColourBlock3(final Vec start, final Vec end, final int[] indices, final byte[] block, final int offset) {
		// get the packed values
		int a = floatTo565(start);
		int b = floatTo565(end);

		// remap the indices
		if (a <= b) {
			// use the indices directly
			System.arraycopy(indices, 0, remapped, 0, 16);
		} else {
			// swap a and b
			final int tmp = a;
			a = b;
			b = tmp;
			for (int i = 0; i < 16; ++i) {
				if (indices[i] == 0)
					remapped[i] = 1;
				else if (indices[i] == 1)
					remapped[i] = 0;
				else
					remapped[i] = indices[i];
			}
		}

		// write the block
		writeColourBlock(a, b, remapped, block, offset);
	}

	static void writeColourBlock4(final Vec start, final Vec end, final int[] indices, final byte[] block, final int offset) {
		// get the packed values
		int a = floatTo565(start);
		int b = floatTo565(end);

		// remap the indices

		if (a < b) {
			// swap a and b
			final int tmp = a;
			a = b;
			b = tmp;
			for (int i = 0; i < 16; ++i)
				remapped[i] = (indices[i] ^ 0x1) & 0x3;
		} else if (a == b) {
			// use index 0
			Arrays.fill(remapped, 0);
		} else {
			// use the indices directly
			System.arraycopy(indices, 0, remapped, 0, 16);
		}

		// write the block
		writeColourBlock(a, b, remapped, block, offset);
	}
}