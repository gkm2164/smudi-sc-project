import Dependencies._

name := "tile-state-persistence"

libraryDependencies ++= Seq(
  `akka-persistence`,
  `akka-persistence-redis`,
  `akka-cluster`,
  `akka-cluster-tool`,
  `rediscala`,
  jodatime,
  jodaconv,
  geotrellis,
  spark
)