import Dependencies._

name := "tile-uploader"

//resolvers += Resolver.url("http://dl.bintray.com/jai-imageio/maven/")

libraryDependencies ++= Seq(
  geotrellis,
  spark
  /*,
  `spray-json`,
  "com.github.jai-imageio" % "jai-imageio-core" % "1.3.0",
  "com.github.jai-imageio" % "jai-imageio-jpeg2000" % "1.3.0"*/
)