package com.satreci.smudi.map

import com.satreci.smudi.{CoordSystemDesc, CoordToTileUtil}
import com.satreci.smudi.tile.entity.{TileExtent, TileResolution}
import geotrellis.proj4.LatLng
import geotrellis.raster.{TileExtents, TileLayout}
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.util.{FileRangeReader, StreamingByteReader}
import geotrellis.vector.Extent

object Cutter {
  def main(args: Array[String]): Unit = {
    val path = args(0)
    val stride = args(1).toInt
    val targetPath = args(2)

    val frr = FileRangeReader(path)
    val sbr = StreamingByteReader(frr)

    val tiff = GeoTiffReader.readMultiband(sbr, decompress = false, streaming = true)

    val te = TileExtent(tiff)

    val pext: Extent = tiff.projectedExtent.reproject(LatLng)

    println(pext)
    
    val width = pext.xmax - pext.xmin
    val height = pext.ymax - pext.ymin

    val colSize = width / tiff.tile.cols
    val rowSize = height / tiff.tile.rows

    val level = CoordToTileUtil.findNearestLevel(Math.min(colSize, rowSize))

    val lrc: TileResolution = CoordSystemDesc.levelRowCol(level)

    println(lrc.cols.toInt, lrc.rows.toInt)

    val tileLayout = TileLayout(lrc.cols.toInt, lrc.rows.toInt, 512, 512)
    val extent = Extent(-180, -90, 180, 90)
    val tileExtents = TileExtents(extent, tileLayout)

    println((te.rmin, te.rmax, te.extents.head._2.size))
    
    println("PJ" + tiff.projectedExtent)

    for {
      row <- te.rmin to te.rmax by stride
      col <- te.cmin to te.cmax by stride
    } yield {
      println(s"Processing $row, $col")
      val lu = tileExtents(col, row)
      val rb = tileExtents({
        if (col + stride <= te.cmax) col + stride else te.cmax
      }, {
        if (row + stride <= te.rmax) row + stride else te.rmax
      })

      println(lu, rb)

      val xmin = Math.min(lu.xmin, rb.xmin)
      val xmax = Math.max(lu.xmax, rb.xmax)
      val ymin = Math.min(lu.ymin, rb.ymin)
      val ymax = Math.max(lu.ymax, rb.ymax)

      val ext = Extent(xmin, ymin, xmax, ymax)

      println(s"Cropping $ext")

      val smallTiff = tiff.crop(ext)

      val dir = path.splitAt(path.lastIndexOf('/'))._1

      smallTiff.write(s"$targetPath/$level-$row-$col.tif")
    }
  }
}
