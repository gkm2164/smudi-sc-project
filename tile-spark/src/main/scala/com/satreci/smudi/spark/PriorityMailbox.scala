package com.satreci.smudi.spark

import akka.actor.{ActorSystem, PoisonPill}
import akka.dispatch.{PriorityGenerator, UnboundedStablePriorityMailbox}
import com.satreci.smudi.messages.MergeTile
import com.satreci.smudi.messages.TileStatus.SetTileState
import com.typesafe.config.Config

class PriorityMailbox (settings: ActorSystem.Settings, config: Config)
  extends UnboundedStablePriorityMailbox(
    // Create a new PriorityGenerator, lower prio means more important
    PriorityGenerator {
      case mt: MergeTile => 0
      case st: SetTileState => 0
      case PoisonPill => 3
      case _ => 1
    })