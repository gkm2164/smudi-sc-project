package com.satreci.smudi.spark

import java.net.URI
import java.util.concurrent.ConcurrentLinkedQueue

import akka.actor._
import akka.cluster.client.ClusterClient
import akka.pattern._
import akka.util.Timeout
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.messages.TileStatus._
import geotrellis.raster.io.geotiff.writer.GeoTiffWriter
import geotrellis.util.StreamingByteReader
import org.apache.hadoop.conf.Configuration
import com.satreci.smudi.tile.entity.Implicits._
import com.satreci.smudi.tile.entity.TileCoord
import com.satreci.smudi.actors.ActorAddress._
import com.satreci.smudi.hdfs.HdfsImageFS
import com.satreci.smudi.messages._
import com.typesafe.config.Config
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.spark.io.hadoop._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext

import scala.collection._
import scala.collection.mutable
import scala.collection.convert.decorateAsScala._
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

import java.util.concurrent.ConcurrentHashMap

class TileSpark(catalog: String, cluster: ActorRef)(implicit sc: SparkContext, imageInventory: ImageInventory, config: Config)
  extends Actor with ActorLogging {
  implicit val dispatcher: ExecutionContextExecutor = context.dispatcher

  var waitQueue: concurrent.Map[TileCoord, ConcurrentLinkedQueue[TileState => Unit]] =
    new ConcurrentHashMap[TileCoord, ConcurrentLinkedQueue[TileState => Unit]]().asScala

  override def receive = {
    case TileCreateRequest(level, row, col) => createTile(level, row, col)
    case WaitSingleTile(l, r, c, callback) => enqueueWaitQueue(l, r, c, callback)
    case MergeTile(level, row, col) => mergeTile(level, row, col)
    case TileStateResult(level, row, col, tileState) => fetchQueue(TileCoord(level, row, col), tileState)
    case tileCoord@TileCoord(_, _, _) => sender ! getTile(tileCoord)
    case _ => sender ! false
  }

  private def getTileStateDelegate(level: Int, row: Int, col: Int)(callback: PartialFunction[Any, Unit]): Unit = {
    implicit val timeout: Timeout = akka.util.Timeout(24 hours)

    def getTileStateDelegate(retry: Int): Unit = {
      if (retry < 1)
        log.error("Retry error!")
      else {
        val tc = TileCoord(level, row, col)
        log.info(s"Retry count $retry for $tc")

        cluster ? ClusterClient.Send(TileInventory(catalog), GetTileState(level, row, col), localAffinity = false) onComplete {
          case Success(result) => callback(result)
          case Failure(_) => getTileStateDelegate(retry - 1)
        }
      }
    }

    getTileStateDelegate(3)
  }

  private def enqueueWaitQueue(level: Int, row: Int, col: Int, callback: TileState => Unit): Unit = {
    val tc = TileCoord(level, row, col)

    log.info(s"Adding queue 1 $tc")
    waitQueue.synchronized {
      if (!(waitQueue contains tc))
        waitQueue += tc -> new ConcurrentLinkedQueue[TileState => Unit]()
      waitQueue(tc).add(callback)
    }
    log.info(s"Adding synchronized 1 $tc")

    cluster ! ClusterClient.Send(TileInventory(catalog),
      WaitAvailabilityCluster(catalog, level, row, col), localAffinity = false)
    cluster ! ClusterClient.Send(TileInventory(catalog),
      RefreshTileState(level, row, col), localAffinity = false)
  }

  private def createTile(level: Int, row: Int, col: Int): Unit = {
    implicit val timeout: Timeout = akka.util.Timeout(24 hours)

    def createTile(level: Int, row: Int, col: Int, retry: Int): Unit = {
      if (retry < 1)
        log.error("Retry error!")
      else {
        log.info("Before send a command")

        getTileStateDelegate(level, row, col) {
          case TileState.Available =>
            fetchQueue(TileCoord(level, row, col), TileState.Available)
          case TileState.Unavailable =>
            fetchQueue(TileCoord(level, row, col), TileState.Unavailable)
          case _: TileState =>
            prepareTile(TileCoord(level, row, col))
          case cmd =>
            log.warning(s"Unknown command: $cmd")
        }
      }
    }

    createTile(level, row, col, 3)
  }

  private def fetchQueue(tileCoord: TileCoord, tileState: TileState): Unit = tileState match {
      case TileState.Available | TileState.Unavailable =>
        log.info(s"Fetching Queue for $tileCoord")
        log.info(s"Fetching wait queues.1 $tileCoord")
        val queue = waitQueue.synchronized {
          if (waitQueue.contains(tileCoord)) {
            waitQueue(tileCoord)
          } else {
            null
          }
        }
        if (queue != null) {
          while (!queue.isEmpty) {
            queue.poll()(tileState)
          }
        }
        log.info(s"Fetching wait queues synchronized 1. $tileCoord")

      case _ => // Ignore
    }

  val hdfsHost: String = config.getString("smudi.hadoop.address")

  private def getTile(coord: TileCoord): Option[MultibandGeoTiff] = {
    val (level, row, col) = coord match {
      case TileCoord(l, r, c) => (l, r, c)
    }

    log.info(s"[TileSpark] Tile request for $level, $row, $col")

    try {
      val hdfsPath = new Path(s"$hdfsHost${HdfsImageFS.tilePath(catalog, level, row, col)}")
      val conf = new Configuration()
      val hrr = HdfsRangeReader(hdfsPath, conf)
      val sbr = StreamingByteReader(hrr)

      val result = Some(GeoTiffReader.readMultiband(sbr, decompress = true, streaming = false))
      cluster ! ClusterClient.Send(TileInventory(catalog),
        SetTileState(level, row, col, TileState.Available), localAffinity = false)
      result
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }

  var tileResult: mutable.Map[TileCoord, List[TileStateResult]] = mutable.Map().withDefaultValue(List())

  private def prepareTile(tc: TileCoord): Unit = {
    val (level, row, col) = tc match {
      case TileCoord(l, r, c) => (l, r, c)
    }

    log.info(s"[TileSpark] Tile create request for $level, $row, $col")

    cluster ! ClusterClient.Send(TileInventory(catalog),
      SetTileState(level, row, col, TileState.Preparing), localAffinity = false)

    val urlPath = new Path(HdfsImageFS.tilePath(catalog, level, row, col))
    val conf = new Configuration()
    val hdfs: FileSystem = FileSystem.get(new URI(hdfsHost), conf)

    if (!hdfs.exists(urlPath)) {
      log.warning(s"$urlPath is not exist currently.")
      if (!imageInventory.isValidQuery(RequestQuery(catalog, level, row, col))) {
        cluster ! ClusterClient.Send(TileInventory(catalog),
          SetTileState(level, row, col, TileState.Unavailable), localAffinity = false)
        fetchQueue(tc, TileState.Unavailable)
        log.error("[TileSpark] The tile does not exist")
      } else {
        val childExtents = tc.childTileExtent
        val result = for {
          col <- childExtents.cmin until childExtents.cmax
          row <- childExtents.rmin until childExtents.rmax
        } yield TileCoord(childExtents.level, row, col)


        result.foreach {
          case tuple@TileCoord(l, r, c) =>
            log.info(s"[TileSpark] Wait for the result from $tc to $tuple")
            enqueueWaitQueue(l, r, c, (tileState) => {
              log.info(s"[TileSpark] Callback replied $tuple with ${tileState.toString}")

              val newTileResult: List[TileStateResult] = tileResult.synchronized {
                val result = tileResult(tc) :+ TileStateResult(l, r, c, tileState)
                log.info(s"[TileSpark] tileResult synchronized 1")
                tileResult(tc) = result
                result
              }
              log.info(s"[TileSpark] tileResult synchronized 2")

              val testSet = newTileResult.distinct

              println(s"Current set: $testSet")
              if ((testSet lengthCompare 4) >= 0) {
                if (testSet.forall(_.tileState == TileState.Available)) {
                  log.info(s"[TileSpark] All tiles for $tc is available")
                  mergeTile(level, row, col) // Merge tile request, originally, it called cluster
                  cluster ! ClusterClient.Send(TileInventory(catalog),
                    SetTileState(level, row, col, TileState.Available), localAffinity = false)
                  fetchQueue(TileCoord(level, row, col), TileState.Available)
                } else {
                  log.error(s"[TileSpark] [Failure]: Responsed with ${testSet.map(_.toString)}.")
                  cluster ! ClusterClient.Send(TileInventory(catalog),
                    SetTileState(level, row, col, TileState.Unavailable), localAffinity = false)
                  fetchQueue(TileCoord(level, row, col), TileState.Unavailable)
                }
              }
            })

            log.info(s"[TileSpark] Ask tile create $tuple for $tc")
            createTile(l, r, c) // Merge tile request, originally, it called cluster
        }
      }
    } else {
      cluster ! ClusterClient.Send(TileInventory(catalog),
        SetTileState(level, row, col, TileState.Available), localAffinity = false)
      fetchQueue(tc, TileState.Available)

      log.info(s"[TileSpark] Tile $tc exist")
    }
  }

  private def mergeTile(level: Int, row: Int, col: Int): Unit = {
    log.info(s"[TileSpark] Try to merge tile ${TileCoord(level, row, col)}")

    val tc = TileCoord(level, row, col)

    val result = for {
      col <- tc.childTileExtent.cmin until tc.childTileExtent.cmax
      row <- tc.childTileExtent.rmin until tc.childTileExtent.rmax
    } yield TileCoord(tc.childTileExtent.level, row, col)

    val url = new Path(s"$hdfsHost${HdfsImageFS.tilePath(catalog, level, row, col)}")
    val conf = new Configuration()
    val hdfs: FileSystem = FileSystem.get(new URI(hdfsHost), conf)

    hdfs.listStatus(new Path("/")).foreach(println)
    val dirPath = new Path(s"$hdfsHost${HdfsImageFS.tileDir(catalog, level, row, col, "tile")}")
    if (!hdfs.exists(dirPath))
      hdfs.mkdirs(dirPath)

    assert(result.map {
      case TileCoord(l, r, c) => new Path(s"$hdfsHost${HdfsImageFS.tilePath(catalog, l, r, c)}")
    }.forall(hdfs.exists))

    val thisHdfsHost = hdfsHost
    val thisCatalog = catalog
    /* Tiling */
    val newTiff: MultibandGeoTiff = sc.parallelize(result).map {
      case TileCoord(l, r, c) =>
          val confChild = new Configuration()
          val fileName = s"$thisHdfsHost${HdfsImageFS.tilePath(thisCatalog, l, r, c)}"
          HadoopGeoTiffReader.readMultiband(new Path(fileName),
            decompress = true, streaming = false, None, confChild)
    }.reduce(_ + _).mapTile(_.resample(512, 512))

    log.info(s"Band count for $tc: ${newTiff.tile.bandCount}")
    def writingArea(retry: Int): Unit = {
      if (retry < 1)
        return
      try {
        val tiffStream = hdfs.create(url)
        tiffStream.write(GeoTiffWriter.write(newTiff))
        tiffStream.close()

        if (!hdfs.exists(url)) {
          mergeTile(level, row, col)
          writingArea(retry - 1)
        }
      } catch {
        case e: Exception =>
          writingArea(retry - 1)
      }
    }

    writingArea(3)
  }
}
