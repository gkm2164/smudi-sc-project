package com.satreci.smudi

import java.util.concurrent.ConcurrentHashMap

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorPath, ActorRef, ActorSystem, Address, Props}
import akka.cluster.Cluster
import akka.cluster.client.{ClusterClient, ClusterClientReceptionist, ClusterClientSettings}
import akka.http.scaladsl.model.DateTime
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.satreci.smudi.image.ImageInventory
import com.satreci.smudi.mailbox.MonitorActor
import com.satreci.smudi.messages.LoadCatalog
import com.satreci.smudi.spark.TileSpark
import com.typesafe.config.ConfigFactory
import org.apache.spark.{SparkConf, SparkContext}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.io.StdIn
import collection.JavaConverters._

object Bootstrap extends App {
  implicit val config = if (args.length > 0) {
    ConfigFactory.parseString(s"akka.remote.netty.tcp.port=${args(0)}").withFallback(ConfigFactory.load())
  } else {
    ConfigFactory.load()
  }

  implicit val system: ActorSystem = ActorSystem("smudi-clusters", config = config)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val requestTimeout: Timeout = akka.util.Timeout(10 seconds)

  val seedAddr = Address(
    config.getString("smudi.cluster-seed.protocol"),
    config.getString("smudi.cluster-seed.name"),
    config.getString("smudi.cluster-seed.host"),
    config.getInt("smudi.cluster-seed.port")
  )

  println(seedAddr.toString)

  val initialContacts = Set(
    ActorPath.fromString(s"${seedAddr.toString}/system/receptionist")
  )

  val c = system.actorOf(
    ClusterClient.props(
      ClusterClientSettings(system).withInitialContacts(initialContacts)
    ), "client")

  val conf: SparkConf =
    new SparkConf()
      .setMaster(config.getString("smudi.spark.address"))
      .setAppName(s"TileSpark${DateTime.now}")

  implicit val sc: SparkContext = new SparkContext(conf)

  implicit val imageInventory: ImageInventory = new ImageInventory

  private def makeTileSpark(catalog: String): (String, ActorRef) =
    catalog -> system.actorOf(
      Props(new TileSpark(catalog, c) with MonitorActor)
        .withDispatcher("worker-dispatcher"),
      s"${catalog}TileSpark")

  val catalogs = imageInventory.getCatalogs

  val tileSpark: ConcurrentHashMap[String, ActorRef] = new ConcurrentHashMap[String, ActorRef]()

  val cluster = Cluster(system)

  cluster.join(seedAddr)

  catalogs.map(makeTileSpark).foreach {
    case (key, value) => tileSpark.put(key, value)
  }

  val lists: List[ActorRef] = tileSpark.asScala.values.toList

  lists.foreach(actor => {
    println("Path " + actor.path.toStringWithoutAddress + " is added")
    ClusterClientReceptionist(system).registerService(actor)
  })

  def registerCatalog(catalog: String) = {
    val (_, actor) = makeTileSpark(catalog)
    ClusterClientReceptionist(system).registerService(actor)
  }

  ClusterClientReceptionist(system).registerService(system.actorOf(
    Props(new Actor() {
      override def receive: Receive = {
        case LoadCatalog(catalog) => registerCatalog(catalog)
        case _ => println("Invalid operations")
      }
    }), "SparkLoader"))

  StdIn.readLine()

  println("Terminating System...")

  cluster.leave(seedAddr)

  sc.stop()
  system.terminate()
}