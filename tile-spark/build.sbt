import Dependencies._

name := "tile-spark"

resolvers += Resolver.jcenterRepo // Adds Bintray to resolvers for akka-persistence-redis and rediscala

sparkSubmitSparkArgs ++= Seq(
  "--master", "spark://192.168.40.190:7077",
  "--class", "com.satreci.smudi.Bootstrap",
  "--packages", List(
    geotrellis.toSparkDependency(),
    `akka-http`.toSparkDependency(),
    `akka-persistence`.toSparkDependency(),
    `akka-persistence-redis`.toSparkDependency(),
    `akka-cluster`.toSparkDependency(),
    `akka-cluster-tool`.toSparkDependency(),
    `rediscala`.toSparkDependency(),
    `spray-json`.toSparkDependency(),
    s"com.satreci.smudi:coord-system_2.11:${Versions.tileServerVersion}",
    s"com.satreci.smudi:tile-actors-common_2.11:${Versions.tileServerVersion}"
  ).mkString(","),
  "--repositories", "http://oss.jfrog.org/oss-snapshot-local"
)

libraryDependencies ++= Seq(
  `akka-http` % "provided",
  `akka-persistence` % "provided",
  `akka-cluster` % "provided",
  `akka-cluster-tool` % "provided",
  `rediscala` % "provided",
  geotrellis % "provided",
  `spray-json` % "provided",
  "org.apache.spark" %% "spark-core" % "2.1.0" % "provided"
)

resourceDirectory in Compile := baseDirectory.value / "src/main/resources"
