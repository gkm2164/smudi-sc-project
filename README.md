# README #

지도 타일 서버

### What is this repository for? ###

* Scala, Spark등을 활용한 지도 타일 서버
* 0.1

### Projects ###

* Some important projects are as follows
  * server-endpoint: Endpoint of the server
  * tile-spark: Spark endpoint server

### How do I get set up? ###

  1. Basic package to be install

    * Hadoop
        * [Link](http://hadoop.apache.org)
        * Single Cluster 설치
        * 2.8.3에서 동작 확인
        * 9000 포트 사용
  
    * Spark
        * [Link](http://spark.apache.org)
        * 2.1.1 설치
        * 7077 사용

    * Redis
        * [Link](http://redis.io)
        * 버전 무관
        * 6379포트 사용
    
    * GeoTrellis
  
            $ cd /proper/path/to/geotrellis
            # maybe repository would be private...
            $ git clone https://bitbucket.org/smudi/geotrellis.git
            $ cd geotrellis
            $ sbt publish-local


  2. Install SMUDI source codes from repository
- Geotrellis setting
   * Download GeoTrellis 1.1.0 and patch
   * Publish locally (version: GeoTrellis 1.2.0-SNAPSHOT)
   * Special version of this GeoTrellis is in [link](https://gkm2164@bitbucket.org/smudi/geotrellis.git)
   
- Following commands are good points to start
  1. GeoTrellis
```bash
# cd /path/to/geotrellis
# sbt publish-local
```
  2. Configuration and Start
```bash
# cd /path/to/smudi-tile-server
# sbt makeConfig [IP-Address of server]
# sbt "project coord-system" publish-local
# sbt "project tile-actors-common" publish-local
```

         $ cd /proper/path/to/smudi-sc-project
         $ git clone https://bitbucket.org/gkm2164/smudi-sc-project.git

  3. Configuration and Start
  
         $ cd /path/to/smudi-tile-server
         $ sbt makeConfig [IP-address]
         $ sbt "project coord-system" publish-local
         $ sbt "project tile-actors-common" publish-local

  4. Start program

         # Start main software
         $ ./main.sh
         
         # Start in-memory Database manager
         # Hadoop이 실행되어 있어야 함(1.1 참조)
         # Redis가 실행되어야 함(1.3 참조)
         $ ./imdb.sh
         
         # Start spark tiling tool
         # Hadoop이 실행되어 있어야 함(1.1 참조)
         # Spark가 실행되어야 함(1.2 참조)
         $ ./spark.sh
   
### Contribution guidelines ###

* 없음

### Who do I talk to? ###

* TBD.
