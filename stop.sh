#!/usr/bin/env bash

tmux kill-window -t tile-s0
tmux kill-window -t tile-s1
tmux kill-window -t tile-s2
tmux kill-window -t tile-s3

rm ~/redis/6379/dump.rdb
