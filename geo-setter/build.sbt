import Versions._
import Dependencies._

name := "geo-setter"

libraryDependencies ++= Seq(
  geotrellis,
  `spray-json`,
  scalatic,
  scalatest
)

initialCommands +=
  """
    |import geotrellis.proj4._
    |import geotrellis.raster.io.geotiff._
    |import geotrellis.raster.io.geotiff.reader._
    |import geotrellis.util._
    |import geotrellis.vector._
    |import com.satreci.smudi._
    |import com.satreci.smudi.SMUDICoordSystem._
  """.stripMargin