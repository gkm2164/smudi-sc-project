package com.satreci.smudi

import java.io.File

import geotrellis.proj4.LatLng
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.io.geotiff.reader.GeoTiffReader
import geotrellis.raster.io.geotiff.writer.GeoTiffWriter
import geotrellis.util.{FileRangeReader, StreamingByteReader}
import geotrellis.vector.Extent

object GeoSetter {
  def main(args: Array[String]): Unit = {
    val input = args(0)
    val output = args(1)

    val filePattern = "TrueMarble_250m_21600x21600_([ABCDEFGH])([1234]).tif".r

    new File(input).listFiles.foreach {
      fs => {
        val name = fs.getName.toString

	if (name.endsWith("tif")) {

        println(s"Processing $input/$name")

        val (c, r) = name match {
          case filePattern(c_, r_) => ("ABCDEFGH".indexOf(c_), "1234".indexOf(r_))
          case _ => (-1, -1)
        }

        val cmin = (360.0 / 8) * c - 180
        val cmax = (360.0 / 8) * (c + 1) - 180
        val rmin = 90 - (180.0 / 4) * (r + 1)
        val rmax = 90 - (180.0 / 4) * r

        val itiff = GeoTiffReader.readMultiband(StreamingByteReader(FileRangeReader(input + "/" + name)), decompress = false, streaming = true)
        val otiff = MultibandGeoTiff(itiff.tile, Extent(cmin, rmin, cmax, rmax), LatLng)

        GeoTiffWriter.write(otiff, output + "/" + name)
        }
      }
    }




  }
}
