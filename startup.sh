#!/usr/bin/env bash

sbt makeConfig

sbt "project coord-system" publish-local
sbt "project tile-actors-common" publish-local

tmux new-session -d -s tile-s0 'cd ~/redis/6379;redis-server'
tmux new-session -d -s tile-s1 'cd ~/smudi;sbt "project tile-spark-cluster" run'
tmux new-session -d -s tile-s2 'cd ~/smudi;sbt "project tile-state-persistence" run'
tmux new-session -d -s tile-s3 'cd ~/smudi;sbt "project tile-spark" sparkSubmit'
