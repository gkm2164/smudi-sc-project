package com.satreci.smudi.test

import com.satreci.smudi.CoordSystemDesc
import org.scalatest.FlatSpec

class TestCoordSystem extends FlatSpec {
  "[T01]" should "have good base information in infinite stream" in {
    assert(CoordSystemDesc.levelRowColStream.slice(1, 11).toList == CoordSystemDesc.levelRowCol.slice(1, 11).toList)
    assert(CoordSystemDesc.worldMapResolution.slice(1, 11).toList == CoordSystemDesc.worldMapResolutionStream.slice(1, 11).toList)
  }
}
