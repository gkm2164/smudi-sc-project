package com.satreci.smudi

import com.satreci.smudi.CoordSystemDesc._
import com.satreci.smudi.tile.entity.{TileCellWidth, TileCoord}
import geotrellis.proj4.LatLng
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.vector.Extent

object CoordToTileUtil {
  /*
  *
  * Return most nearest level for given Degrees per pixel
  *
  * */
  def findNearestLevel(cellWidth: Double): Int = {
    def fnl(cellWidth: Double, remain: List[TileCellWidth]): Int =
      if (remain.head.level == -1)
        -1
      else if (cellWidth >= remain.head.cellWidth)
        remain.head.level - 1
      else
        fnl(cellWidth, remain.tail)

    if (cellWidth > worldMapResolution.head.cellWidth || cellWidth < worldMapResolution.last.cellWidth) {
      -1
    } else {
      fnl(cellWidth, worldMapResolution)
    }
  }

  def stepwiseIndex(value: Double, step: Double): Int = ((value - value % step) / step).toInt

  def getTileCoord(coord: (Double, Double), level: Int): TileCoord = {
    val (lng, lat) = coord

    val reCalcRes = worldMapResolution(level).cellWidth

    val row = stepwiseIndex(-lat + 90, reCalcRes * tileSize)
    val col = stepwiseIndex(lng + 180, reCalcRes * tileSize)

    TileCoord(level, row, col)
  }

  def getResolution(tiff: MultibandGeoTiff): (Double, Double) = {
    val cols = tiff.tile.cols
    val rows = tiff.tile.rows

    val (lng0, lat0, lng1, lat1) = Extent.unapply(tiff.projectedExtent.reproject(LatLng)).get

    ((lng1 - lng0) / cols, (lat1 - lat0) / rows)
  }
}
