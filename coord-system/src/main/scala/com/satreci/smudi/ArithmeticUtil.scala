package com.satreci.smudi

object ArithmeticUtil {
  val correctionNum = 0.000000000001

  def errorCorrection(num: Double): Double = num - num % correctionNum
}
