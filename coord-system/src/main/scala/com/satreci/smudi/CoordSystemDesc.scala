package com.satreci.smudi

import com.satreci.smudi.tile.entity.{TileCellWidth, TileResolution}

object CoordSystemDesc {
  val tileSize = 512
  val levelRowCol: List[TileResolution] =
    TileResolution(0, 0d, 0d) :: (1 to 25).map(n => TileResolution(n, 0.5 * (1 << (n - 1)), 0.25 * (1 << (n - 1)))).toList
  val worldMapResolution: List[TileCellWidth] = levelRowCol.map(tuple => TileCellWidth(tuple.level, 360d / (tuple.cols * tileSize)))

  val levelRowColStream: Stream[TileResolution] =
    TileResolution(0, 0.25d, 0.125d) #:: levelRowColStream.map {
      case TileResolution(l, c, r) => TileResolution(l + 1, c * 2, r * 2)
    }

  val worldMapResolutionStream: Stream[TileCellWidth] = levelRowColStream.map {
    case TileResolution(l, c, _) => TileCellWidth(l, 360d / (c * tileSize))
  }
}
