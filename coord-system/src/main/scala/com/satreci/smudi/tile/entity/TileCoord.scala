package com.satreci.smudi.tile.entity

import geotrellis.proj4.LatLng
import geotrellis.vector.{Extent, ProjectedExtent}

case class TileCoord(level: Int, row: Int, col: Int) {
  private val serialVersionUID = 1L

  import com.satreci.smudi.CoordSystemDesc._

  lazy val childTileExtent: TileExtent = getChildExtent

  def resolution: Double = worldMapResolution(level).cellWidth
  def tilePointToLngLat: (Double, Double) = {
    val step = resolution * tileSize

    (col * step - 180, 90 - row * step)
  }

  import com.satreci.smudi.ArithmeticUtil._

  def getTileExtent(implicit other: TileCoord = TileCoord(level, row + 1, col + 1)): ProjectedExtent = {
    (tilePointToLngLat, other.tilePointToLngLat) match {
      case ((a, b), (c, d)) => ProjectedExtent(
        Extent(errorCorrection(Math.min(a, c)), errorCorrection(Math.min(b, d)),
          errorCorrection(Math.max(a, c)), errorCorrection(Math.max(b, d))), LatLng)
    }
  }

  def getChildExtent: TileExtent = {
    val from = TileCoord(level + 1, row * 2, col * 2)
    val to = TileCoord(level + 1, row * 2 + 2, col * 2 + 2)

    TileExtent(level + 1, from, to)
  }
}
