package com.satreci.smudi.tile.entity

import com.satreci.smudi.CoordSystemDesc._
import com.satreci.smudi.CoordToTileUtil._
import geotrellis.proj4.LatLng
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.vector.{Extent, ProjectedExtent}
import com.satreci.smudi.ArithmeticUtil._
import spray.json.{DefaultJsonProtocol, JsNumber, JsObject, JsValue, RootJsonFormat}

object NullTileExtent extends TileExtent(0, 0, 0, 0, 0)

object TileExtentJsonProtocol extends DefaultJsonProtocol {

  implicit object TileExtentJsonFormat extends RootJsonFormat[TileExtent] {
    override def write(obj: TileExtent) =
      JsObject(
        "level" -> JsNumber(obj.level),
        "rmin" -> JsNumber(obj.rmin),
        "cmin" -> JsNumber(obj.cmin),
        "rmax" -> JsNumber(obj.rmax),
        "cmax" -> JsNumber(obj.cmax)
      )

    override def read(json: JsValue): TileExtent =
      json.asJsObject.getFields("level", "rmin", "cmin", "rmax", "cmax") match {
        case Seq(JsNumber(level), JsNumber(rmin), JsNumber(cmin), JsNumber(rmax), JsNumber(cmax)) =>
          TileExtent(level.toInt, rmin.toInt, cmin.toInt, rmax.toInt, cmax.toInt)
        case _ => NullTileExtent
      }
  }

}

case class TileExtent(level: Int, rmin: Int, cmin: Int, rmax: Int, cmax: Int) {
  type TileExtentMap = Map[Int, Map[Int, ProjectedExtent]]

  val from = TileCoord(level, rmin, cmin)
  val to = TileCoord(level, rmax, cmax)
  val extents: TileExtentMap = getTileExtents

  import com.satreci.smudi.tile.entity.Implicits._

  lazy val prevLevelExtents: TileExtent =
    if (rmax - rmin > 1 && cmax - cmin > 1 && level > 0)
      TileExtent(level - 1, from.getTileExtent + to.getTileExtent)
    else
      NullTileExtent

  def resolution: Double = worldMapResolution(level).cellWidth

  def getTileExtents: TileExtentMap = {
    val tiles = for {
      row <- from.row until to.row
      col <- from.col until to.col
    } yield (row, col, TileCoord(level, row, col).getTileExtent)

    tiles.groupBy(_._1).mapValues(_.groupBy(_._2).mapValues { case Seq(x) => x._3 })
  }

  def getProjectedExtent: ProjectedExtent = {
    val fromLL = from.tilePointToLngLat
    val toLL = to.tilePointToLngLat

    ProjectedExtent((fromLL, toLL) match {
      case ((a, b), (c, d)) =>
        Extent(errorCorrection(Math.min(a, c)), errorCorrection(Math.min(b, d)), errorCorrection(Math.max(a, c)), errorCorrection(Math.max(b, d)))
    }, LatLng)
  }
}

object TileExtent {
  def apply(level: Int, c0: TileCoord, c1: TileCoord): TileExtent  = TileExtent(level, c0.row, c0.col, c1.row, c1.col)
  def apply(resolution: Double, c0: TileCoord, c1: TileCoord): TileExtent  = TileExtent(findNearestLevel(resolution), c0, c1)
  def apply(level: Int, pext: ProjectedExtent): TileExtent  = {
    val extent = pext.reproject(LatLng)

    val from = getTileCoord((extent.xmin, extent.ymax), level)
    val to = getTileCoord((extent.xmax, extent.ymin), level)

    TileExtent(level, from, to)
  }

  def apply(resolution: Double, pext: ProjectedExtent): TileExtent = TileExtent(findNearestLevel(resolution), pext)

  def apply(tiff: MultibandGeoTiff): TileExtent = {
    val llExtent = tiff.projectedExtent.reproject(LatLng)

    val tiffCols = tiff.tile.cols
    val tiffRows = tiff.tile.rows

    val (_cellHeight, _cellWidth) = ((llExtent.ymax - llExtent.ymin) / tiffRows, (llExtent.xmax - llExtent.xmin) / tiffCols)

    val (cellHeight, cellWidth) = (errorCorrection(_cellHeight), errorCorrection(_cellWidth))

    val maxRes = Math.min(cellHeight, cellWidth)

    TileExtent(maxRes, tiff.projectedExtent)
  }
}