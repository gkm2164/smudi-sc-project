package com.satreci.smudi.tile.entity

case class TileCellWidth(level: Int, cellWidth: Double)

sealed abstract class TileResolutionEntity
case class TileResolution(level: Int, cols: Double, rows: Double) extends TileResolutionEntity
case object NullTileResolution extends TileResolutionEntity