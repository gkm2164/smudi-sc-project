package com.satreci.smudi.tile.entity

import com.satreci.smudi.CoordSystemDesc
import geotrellis.proj4.LatLng
import geotrellis.raster.{ArrayMultibandTile, CellSize, RasterExtent}
import geotrellis.raster.io.geotiff.MultibandGeoTiff
import geotrellis.raster.resample.Bilinear
import geotrellis.vector.{Extent, ProjectedExtent}

object Implicits {
  implicit def extendPEAlgebra(pe: ProjectedExtent) = new {
    def +(that: ProjectedExtent): ProjectedExtent = {
      val crs = pe.crs
      val extent = pe.extent
      val newThat = that.reproject(crs)

      val _xmin = Math.min(extent.xmin, newThat.xmin)
      val _xmax = Math.max(extent.xmax, newThat.xmax)
      val _ymin = Math.min(extent.ymin, newThat.ymin)
      val _ymax = Math.max(extent.ymax, newThat.ymax)

      ProjectedExtent(Extent(xmin = _xmin, ymin = _ymin, xmax = _xmax, ymax = _ymax), crs)
    }
  }

  implicit def extendMergeTileMethod(tiff0: MultibandGeoTiff) = new {
    def +(tiff1: MultibandGeoTiff): MultibandGeoTiff = {
      val tile0 = tiff0.tile
      val tile1 = tiff1.tile

      val info0 = TileExtent(tiff0)
      val info1 = TileExtent(tiff1)

      val l0 = info0.level
      val l1 = info1.level

      val minLevel = Math.max(l0, l1)
      val cellSize = CoordSystemDesc.worldMapResolution(minLevel)
      val cs = cellSize.cellWidth

      val newpe = tiff0.projectedExtent + tiff1.projectedExtent

      val wholeWorld = ProjectedExtent(Extent(-180, -90, 180, 90), LatLng).reproject(newpe.crs)

      val lrc = CoordSystemDesc.levelRowCol(minLevel)

      val re = RasterExtent(newpe.reproject(LatLng), CellSize(cs, cs))

      val amt = ArrayMultibandTile.alloc(tiff0.cellType, tile0.bandCount, re.cols, re.rows)

      val newTile = amt.merge(newpe.reproject(LatLng), tiff0.projectedExtent.reproject(LatLng), tile0, Bilinear)
      val newTile2 = newTile.merge(newpe.reproject(LatLng), tiff1.projectedExtent.reproject(LatLng), tile1, Bilinear)

      MultibandGeoTiff(newTile2, newpe.extent, newpe.crs)
    }
  }
}
